package music;

import java.util.LinkedList;

public class DynamicPart extends Part {
	MusicPiece music;

	int job = 0; // 0--> create, 1 --> replay, 2 --> bass, 3 --> harmonize
	int run = 0;
	int time = 0;

	int id = 0;

	LinkedList<Note> hist = new LinkedList<Note>();

	public DynamicPart(int n, MusicPiece m, int id) {
		super(n);

		this.music = m;
		this.id = id;
	}

	@Override
	public Note nextNote() {
		int chosen = 0, dur = 0, strength = 0;
		int[] possible = Chord.TYPE_LOOKUP[music.last.type];

		switch (job) {
		case 0:
		default:
			if (run == 0) {
				if (Math.random() < 0.0001) {
					run = 2 * (int) (Math.random() * 2) - 1;
				}
				// Usually gets a chord note
				chosen = music.last.root + possible[(int) (Math.random() * possible.length)] + 12
						* (int) (Math.random() * 3 - 1);
				double POW = Math.pow(2, (int) (Math.random() * 5 - 2));
				dur = (int) (300 * POW);
				strength = (int) (75 + Math.random() * 75);
			} else {
				chosen = hist.getLast().pitch - music.key + run;
				dur = 75;
				strength = 80;
				if (mod12(chosen) == mod12(music.last.root)) {
					run = 0;
					chosen = chosen - mod12(chosen - music.last.root);
					dur = 600 - (time % 300);
					strength = 100;
				}
			}
			break;
		case 2:
			chosen = music.last.root + possible[id % possible.length] - 24 + 12 * (id / possible.length);
			double end = (1200 - time % 1200);
			int already = 1200 / (int) end;
			double POW = Math.pow(2, (int) (Math.random() * (5 - already) - (4 - already)));
			dur = (int) (end * POW);
			strength = id == 0 ? 120 : 80;
			break;
		}

		time += dur;
		Note n = new Note(chosen + music.key, (int) (dur / music.flow_speed), (int) (strength * music.flow_volume));
		hist.addLast(n);
		if (hist.size() > 10)
			hist.removeFirst();
		return n;
	}

	@Override
	public boolean isDone() {
		// return Math.random() < 0.1 && mod12(music.last.root) == 0;
		return false;
	}

	private static int mod12(int n) {
		return (n >= 0 ? n % 12 : n % 12 + 12);
	}

}
