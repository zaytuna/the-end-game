package util;

public class Encoder {
	public static String encode(String start) {
		String s = "WORLD^@#$&*" + start + "262ORANG�S";// eleven before and
														// ten after
		StringBuilder build = new StringBuilder();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			build.append((char) ((int) c << 2));
		}

		return build.toString();
	}

	public static String encode(int i) {
		return encode(i + "");
	}

	public static String decode(int i) {
		return decode(i + "");
	}

	public static String decode(String start) {
		String s = start.substring(11, start.length() - 10);
		StringBuilder build = new StringBuilder();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			build.append((char) ((int) c >> 2));
		}

		return build.toString();
	}

	public static void main(String[] args) {
		System.out.println(decode(encode("1:05")));
	}
}