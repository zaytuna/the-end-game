package objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashSet;

import math.Vector2D;
import math.Vector3D;
import base.Eye;
import base.TEPanel;

public class LockWall extends HWall {
	double rotate = 0;

	public LockWall(double a, double b, double c, double d) {
		super(a, b, c, d);
		operational = true;
	}

	@Override
	public ArrayList<GameShape> create(double h) {
		// return super.create(h);
		ArrayList<GameShape> shapes = new ArrayList<GameShape>();
		if (operational) {
			double dx = 0.5 / Vector2D.dist(x1, y1, x2, y2);
			for (double d = 0; d < 1; d += dx * 1) {
				if (1 - d < dx)
					dx = 1 - d;

				shapes.add(new GameShape(color, false, lini(x1, x2, d), lini(y1, y2, d), 0, lini(x1, x2, d + dx), lini(
						y1, y2, dx + d), 0, lini(x1, x2, dx + d), lini(y1, y2, d + dx), h, lini(x1, x2, d), lini(y1,
						y2, d), h));
			}
		} else {
// shapes.add(new GameShape(Methods.getColor(color, 100), false, x1, y1, 0.1,
// x2, y2, 0.1, x2, y2, h, x1, y1,
// h));
		}
		Vector2D p = new Vector2D(x2 - x1, y2 - y1).perp().scale(0.2);
		double H = -.1;
		shapes.add(new GameShape(color, true, x1 + p.x, y1 + p.y, H, x2 + p.x, y2 + p.y, H, x2, y2, 0, x1, y1, H));
		shapes.add(new GameShape(color, true, x1, y1, H, x2, y2, H, x2 - p.x, y2 - p.y, H, x1 - p.x, y1 - p.y, H));

		children.clear();
		children.addAll(shapes);
		System.out.println(children);
		return shapes;
	}

	public void draw(Graphics2D g, Eye e) {
		super.draw(g, e);
		g.setColor(e.transform(color, new Vector3D(Math.cos(rotate), Math.sin(rotate), 1), 0));

		Vector3D v1 = e.toScreen(new Vector3D(x1, y1, height)), v2 = e.toScreen(new Vector3D(x2, y2, height));

		if (v1 != null && v2 != null) {

			// **************** STUFF FOR HOLE DRAWING ********************
			Vector3D center = new Vector3D((x1 + x2) / 2, (y1 + y1) / 2, height);

			Vector3D v = e.toScreen(center);
			int w = (int) e.sphereWidth(center, 0.1) + 1, h = (int) (Math.abs(w * Math.cos(e.beta))) + 1;

			g.setStroke(new BasicStroke(2));
			g.setColor(e.transform(Color.WHITE, new Vector3D(x2 - x1, y2 - y1, 0), 1));
			g.drawOval(e.psX(v.x) - w, e.psY(v.y) - h, 2 * w, 2 * h);

			g.drawPolygon(new int[] { e.psX(v.x), (int) (e.psX(v.x) + w * 2 * Math.cos(rotate)),
					(int) (e.psX(v.x) + w * 2 * Math.cos(rotate + 0.5)) },
					new int[] { e.psY(v.y), (int) (e.psY(v.y) + h * 2 * Math.sin(rotate)),
							(int) (e.psY(v.y) + h * 2 * Math.sin(rotate + 0.5)) }, 3);

			g.setColor(Color.BLACK);
			g.fillOval(e.psX(v.x) - w + 1, e.psY(v.y) - h + 1, 2 * w - 2, 2 * h - 2);
			g.fillPolygon(new int[] { e.psX(v.x), (int) (e.psX(v.x) + w * 2 * Math.cos(rotate)),
					(int) (e.psX(v.x) + w * 2 * Math.cos(rotate + 0.5)) },
					new int[] { e.psY(v.y), (int) (e.psY(v.y) + h * 2 * Math.sin(rotate)),
							(int) (e.psY(v.y) + h * 2 * Math.sin(rotate + 0.5)) }, 3);

			g.setStroke(new BasicStroke(1));
		}
	}

	@Override
	public void update(HashSet<Integer> keyDown, TEPanel t, double f) {
		rotate += 0.01;
	}

	public static LockWall read(String str) {
		String[] pieces = str.split("\t");
		// System.out.println(str);
		LockWall w = new LockWall(Double.parseDouble(pieces[0]), Double.parseDouble(pieces[1]),
				Double.parseDouble(pieces[2]), Double.parseDouble(pieces[3]));

		w.friction = Double.parseDouble(pieces[4]);
		w.width = Float.parseFloat(pieces[5]);
		w.color = new Color(Integer.parseInt(pieces[6]), true);
		w.bounce = Double.parseDouble(pieces[7]);
		w.setOperational(true);

		return w;
	}

}
