package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URLConnection;

import javax.swing.JOptionPane;

public class IOMaster {
	public static String CONFIG = "data.cfg";
	private static IOMaster single = new IOMaster();

	private int iotype = -1;

	private IOMaster() {
		if (new File(CONFIG).exists())
			iotype = 0;
		else if (getClass().getClassLoader().getResource(CONFIG) != null)
			iotype = 1;
	}

	public static String getFileContents(String str) {
		try {
			Reader reader = null;
			if (single.iotype == 0)
				reader = new FileReader(str);
			else if (single.iotype == 1) {
				reader = new InputStreamReader(single.getClass().getClassLoader().getResource(str).openStream());
			}

			StringBuilder sb = new StringBuilder();

			BufferedReader in = new BufferedReader(reader);
			while (true) {
				String temp = in.readLine();
				if (temp == null)
					break;
				sb.append(temp + "\n");
			}
			in.close();
			return sb.toString();
		} catch (Exception e) {
			StackTraceElement[] s = e.getStackTrace();
			String string = "";
			for (int i = 0; i < s.length; i++) {
				string += "\n\tAt " + s[i].toString();
			}
			JOptionPane.showMessageDialog(null, "Input type: " + single.iotype + "\nERROR: " + e + "\n\n" + string,
					"GG", 0);
			e.printStackTrace();
		}
		return null;
	}

	public static void writeToFile(String rsc, String str) {
		try {
			Writer writer = null;
			if (single.iotype == 0)
				writer = new FileWriter(rsc);
			else if (single.iotype == 1) {
				URLConnection connection = single.getClass().getClassLoader().getResource(str).openConnection();
				connection.setDoOutput(true);
				writer = new OutputStreamWriter(connection.getOutputStream());
			}
			BufferedWriter out = new BufferedWriter(writer);
			out.write(str);
			out.flush();
			out.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "GG: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
