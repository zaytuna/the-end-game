package testbed;

import java.awt.Color;

import javax.swing.JColorChooser;

public class ColorPrinter {

	public static void main(String[] args) {
		Color color = Color.white;

		while (true) {
			color = JColorChooser.showDialog(null, "", color);
			System.out.println(color.getRGB());
		}
	}
}
