package base;

import java.util.ArrayList;

public abstract class Trigger implements Reconstructable {
	public ArrayList<Action> attached = new ArrayList<Action>();
	protected boolean armed = true;

	public abstract boolean condition(Player p);

	public void fire(TEPanel tp) {
		System.out.println("FIRING ");
		for (Action a : attached) {
			a.perform(tp);
		}
	}

	public boolean isActive(Player p) {
		boolean b = condition(p), c = armed && b;
		armed = !b;
		return c;
	}

}
