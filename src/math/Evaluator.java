package math;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

public class Evaluator {
	public static HashMap<String, Double> constants = new HashMap<String, Double>();

	static {
		constants.put("pi", Math.PI);
		constants.put("e", Math.E);
	}

	public static Expression createFromHelper(String str, HashMap<String, Integer> map) {
		for (int parenthesisLevel = 0; parenthesisLevel <= maxParLevel(str); parenthesisLevel++) {
			for (Operation op : Operation.values()) {
				int itterator = 0;

				while (containsIgnoreCase(op.sign, str, itterator)
						&& getParLevel(str, str.indexOf(op.sign, itterator)) > parenthesisLevel)
					itterator = str.indexOf(op.sign, itterator) + 1;

				if (containsIgnoreCase(op.sign, str, itterator)
						&& getParLevel(str, str.indexOf(op.sign, itterator)) <= parenthesisLevel) {

					if (op == Operation.SUB
							&& (str.indexOf(op.sign, itterator) <= 0 || goBackwards(str,
									str.indexOf(op.sign, itterator) - 1) == 0))
						continue;// for negative numbers

					if (op.position == -1) {
						Expression a = createFromHelper(str.substring(0, str.indexOf(op.sign, itterator)), map);
						Expression b = createFromHelper(
								str.substring(str.indexOf(op.sign, itterator) + op.sign.length()), map);
						Expression c = new Expression(op, a, b);
						return c;
					} else if (op.position == -2) {
						ArrayList<Expression> expr = new ArrayList<Expression>();

						int holder = 0;
						while (str.indexOf(",", holder) > 0 && str.indexOf(",", holder) < str.indexOf(op.sign)) {
							expr.add(createFromHelper(str.substring(holder, str.indexOf(",")), map));
							holder = str.indexOf(",") + 1;
						}

						expr.add(createFromHelper(str.substring(holder, str.indexOf(op.sign, itterator)), map));

						return new Expression(op, expr);
					} else if (op.position >= 0) {
						ArrayList<Expression> expr = new ArrayList<Expression>();

						int holder = str.indexOf(op.sign) + op.sign.length();
						while (str.indexOf(",", holder) > 0) {
							expr.add(createFromHelper(str.substring(holder, str.indexOf(",")), map));
							holder = str.indexOf(",") + 1;
						}

						expr.add(createFromHelper(str.substring(holder, str.length()), map));

						return new Expression(op, expr);
					}
				}
			}
		}

		str = str.replaceAll("\\(", "").replaceAll("\\)", "").trim();

		try {
			double d = Double.parseDouble(str.trim());
			return new Expression(d);
		} catch (Exception e) {
			if (constants.containsKey(str.toLowerCase()))
				return new Expression(constants.get(str.toLowerCase()));

			System.out.println("CREATING VAR: \"" + str + "\" --> " + map.get(str));
			if (map.containsKey(str))
				return Expression.createVar(str, map.get(str));
			else {
				map.put(str, map.size());
				return Expression.createVar(str, map.size() - 1);
			}
		}

	}

	private static int goBackwards(String str, int index) {
		while (index >= 0 && (str.charAt(index) == ' '))
			index--;

		if (str.charAt(index) == '(')
			return 0;

		if (Character.isDigit(str.charAt(index)))
			return 1;

		for (Operation op : Operation.values()) {
			boolean broken = false;
			for (int test = index; test >= 0 && index - test < op.sign.length(); test--)
				if (str.charAt(test) != op.sign.charAt(index - test))
					broken = true;

			if (!broken)
				return 0;
		}

		return 2;
	}

	private static int maxParLevel(String str) {
		int level = 0, max = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == '(') {
				if (++level >= max)
					max = level;
			} else if (str.charAt(i) == ')')
				level--;
		}

		return max;
	}

	private static int getParLevel(String str, int index) {
		int level = 0;
		for (int i = 0; i <= index; i++)
			if (str.charAt(i) == '(')
				level++;
			else if (str.charAt(i) == ')')
				level--;

		return level;
	}

	public static boolean containsIgnoreCase(String test, String entire, int itterator) {
		int index = 0;

		for (int i = itterator; i < entire.length(); i++) {
			if (Character.toLowerCase(test.charAt(index)) == Character.toLowerCase(entire.charAt(i)))
				index++;
			if (index >= test.length()) {
				System.out.println("contains: " + test + "\t" + entire + "\t" + itterator);

				return true;
			}
		}
		return false;
	}

	public static Expression createFrom(String arg) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		return createFromHelper(arg, map);
	}

	public static Expression createFrom(String arg, HashMap<String, Integer> map) {
		return createFromHelper(arg, map);
	}

	public static void main(String[] args) {
		while (true) {
			String str = JOptionPane.showInputDialog("Enter Expression");
			if (str == null)
				break;
			Expression exp = createFrom(str);
			str += "\ninterpreted as: " + exp + "\nevaluated with vars (1,2,3): " + exp.evaluate(1, 2, 3) + "\n";
			str += exp.treeForm();
			exp.trySimplify();
			str += "\n" + exp;
			JOptionPane.showMessageDialog(null, str);
			System.out.println();
		}
	}
}