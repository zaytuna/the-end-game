package objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import util.Methods;
import base.Action;
import base.Overlay;
import base.TEPanel;
import base.Trigger;

public class BlockingMessage extends Action implements Overlay {
	public String text;
	public Font font;
	public Color color;
	private boolean dead = false;

	public BlockingMessage(Trigger t, String str) {
		super(t);
		color = Color.WHITE;
		font = new Font("SANS_SERIF", Font.PLAIN, 60);
		text = str;
	}

	private BlockingMessage() {
		super(null);
	}

	@Override
	public String pack() {
		return text.replaceAll("\n", "$$").replaceAll("\t", "##") + "\t" + color.getRGB() + "\t" + font.getName()
				+ "\t" + font.getSize() + "\t"
				+ ((font.isBold() ? Font.BOLD : Font.PLAIN) + (font.isItalic() ? Font.ITALIC : Font.PLAIN));
	}

	public static BlockingMessage fromString(String str) {
		BlockingMessage msg = new BlockingMessage();
		String[] pieces = str.split("\t");

		msg.text = pieces[0].replaceAll("$$", "\n").replaceAll("##", "\t");
		msg.color = new Color(Integer.parseInt(pieces[1]), true);
		msg.font = new Font(pieces[2], Integer.parseInt(pieces[4]), Integer.parseInt(pieces[3]));

		return msg;
	}

	@Override
	public void update(TEPanel tp) {
		if (tp.getKeys().contains(java.awt.event.KeyEvent.VK_SPACE)) {
			tp.slowmo = 1;
			dead =  true;
			//tp.overlays.remove(this);
		}
	}

	@Override
	public void perform(TEPanel tp) {
		tp.overlays.add(this);
		tp.slowmo = 0;
	}

	@Override
	public void draw(Graphics2D g, TEPanel te) {
		g.setColor(new Color(0, 0, 0, 200));
		g.fill(g.getClip());
		g.setColor(color);
		g.setFont(font);
		Methods.drawCenteredText(g, text, 0, 0, te.getWidth(), te.getHeight());
	}

	@Override
	public boolean isDead(TEPanel te) {
		if(dead) {
			dead = false;
			return true;
		}
		return false;
	}

	@Override
	public void mousePress(MouseEvent evt) {
		
	}

	@Override
	public void mouseRelease(MouseEvent evt) {
		
	}
}
