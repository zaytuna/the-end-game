package objects;

import java.util.HashMap;

import math.Evaluator;
import math.Expression;
import base.Player;
import base.Trigger;

public class ExpressionTrigger extends Trigger {
	public static final HashMap<String, Integer> ORDER;

	public boolean visible = false;

	Expression truth;
	static {
		ORDER = new HashMap<String, Integer>();
		ORDER.put("x", 0);
		ORDER.put("y", 0);
		ORDER.put("z", 0);
		// put other useful non-position vars here;
	}

	public ExpressionTrigger(String condition) {
		truth = Evaluator.createFrom(condition, ORDER);
	}

	public boolean condition(Player p) {
		return truth.evaluate(p.pos.x, p.pos.y, p.pos.z) > 0;
	}

	@Override
	public String pack() {
		return truth.toString();
	}
}
