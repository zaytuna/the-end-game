package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import base.Eye;
import base.RenderShard;
import math.Vector3D;

public class ExpendableLineRenderShard implements RenderShard {
	int x1, y1, x2, y2;

	Vector3D center, normal;
	Color c;
	Stroke k;

	public ExpendableLineRenderShard(Color c, Stroke k, int x1, int y1, int x2, int y2, Vector3D q, Vector3D n) {
		this.x1 = x1;
		this.x2 = x2;
		this.y2 = y2;
		this.y1 = y1;
		this.k = k;
		this.c = c;
		this.normal = n;
		this.center = q;
	}

	public ExpendableLineRenderShard(int x1, int y1, int x2, int y2, Vector3D q, Vector3D n) {
		this(null, null, x1, y1, x2, y2, q, n);
	}

	@Override
	public void draw(Graphics2D g, Eye e) {
		if (c != null)
			g.setColor(c);
		if (k != null)
			g.setStroke(k);
		g.drawLine(x1, y1, x2, y2);
	}

	@Override
	public Vector3D getCenter() {
		return center;
	}

	@Override
	public Vector3D getNormal() {
		return normal;
	}
}
