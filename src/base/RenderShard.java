package base;

import java.awt.Graphics2D;

import math.Vector3D;

public interface RenderShard {
	public abstract void draw(Graphics2D g, Eye e);

	public abstract Vector3D getCenter();
	public abstract Vector3D getNormal();
}
