package physics;

import java.util.ArrayList;
import java.util.HashSet;

import math.Vector2D;
import math.Vector3D;
import util.Methods;
import base.TEPanel;

public abstract class PolyPObject extends PhysicsObject implements ForceApplier {
	public abstract Vector3D[] getVertices();

	public double angle, omega;

	public abstract void specUpdate(HashSet<Integer> keyDown, TEPanel v, double factor);

	protected ArrayList<Vector3D> pList = new ArrayList<Vector3D>(), fList = new ArrayList<Vector3D>();
	// TEMPORARY USE ONLY, FOR DISPLAY
	protected ArrayList<Vector3D> pList2 = new ArrayList<Vector3D>(), fList2 = new ArrayList<Vector3D>();

	public void applyForce(Vector3D p, Vector3D f) {
		pList.add(p);
		fList.add(f);
	}

	public Vector3D getVVelo(int i) {
		Vector3D[] v = getVertices();
		return velo.clone().add(v[i].clone().perp().scale(omega));
	}

	@Override
	public final void updateU(HashSet<Integer> keyDown, TEPanel v, double factor) {

		super.updateU(keyDown, v, factor);

		omega *= Math.pow(0.98, factor);
		Vector3D center = new Vector3D(), fTot = new Vector3D(), torque = new Vector3D();

		for (int i = 0; i < pList.size(); i++) {
			center.add(pList.get(i));
			fTot.add(fList.get(i));
			torque.add(pList.get(i).cross(fList.get(i)));
		}

		if (pList.size() > 0)
			center.scale(1.0 / pList.size());

		//TODO: Test more complicated physics
		// ** TEST SUPER SIMPLE PHYSICS
		velo.add(fTot.scale(1.0 / mass));
		omega += (torque.z) / rInertia;
		// ** END TEST

		ArrayList<Vector3D> pref = pList2, fref = fList2;

		pList2 = pList;
		fList2 = fList;
		pList = pref;
		fList = fref;

		fList.clear();
		pList.clear();

		// centerVelo(center, fTot, torque);

		specUpdate(keyDown, v, factor);
	}

	/*
	 * 2D ONly right now, but this method should be extended
	 */
	@Override
	public void magic(PhysicsObject o) {
		if (o == this)
			return;
		if (o instanceof PolyPObject) {
			PolyPObject po = (PolyPObject) o;

			Vector3D diff = pos.clone().sub(o.pos);

			if ((o.radius + radius) > diff.magnitude()) {
				// for every pair of edges, do line physics
				Vector3D[] vertices = getVertices();
				for (int i = 0; i < vertices.length; i++) {
					Vector3D v1 = vertices[i], v2 = vertices[(i + 1) % vertices.length];
					linePhysics2D(po, 0.5, v1.x, v1.y, v2.x, v2.y, velo);
				}

			}
		} else {
// assume po is a circle with a given radius. WE ARE STILL A POLYGON.

			Vector3D diff = o.pos.clone().sub(pos);

			// just a bounds check
			if (diff.mag2() < (o.radius + radius) * (o.radius + radius)) {
				Vector3D[] vert = getVertices();
				for (int i = 0; i < vert.length; i++) {
					// consider edge (i,i+1) and the circle
					Vector2D v2 = vert[(i + 1) % vert.length].to2DV();
					Vector2D norm = vert[i].to2DV().minus(v2).negate().perp().unitize();

					Vector2D q = Methods.segmentIntersect(vert[i].to2DV(), v2, Vector2D.ZERO, diff.to2DV());

					if (q != null) {
						double qm = -diff.to2DV().mag() + q.mag() + o.radius;

						if (qm > 0) {
							Vector3D vm = velo.clone().add(q.clone().perp().scale(omega)).sub(o.velo);

							Vector3D f = vm.projOnto(norm).scale(8);

							norm.scale(qm / 2);
							o.pos.add(norm);
							pos.sub(norm);

							o.applyForce(q.to3DV(), f.clone());
							applyForce(q.to3DV().negate(), f.negate());
							
							o.friction = elasticity;
							o.norm = norm.to3DV().normalize();

						}
					}
				}
			}
		}
	}

// /*
// * This takes a force and associated torque (f, t) at position p, and
// * sets (velo, dw) to be apporpritate for a rotation about (pos).
// *
// * ALL poly physics is applicable for 2D only, really.
// */
// private void centerVelo(Vector3D p, Vector3D f, Vector3D torque) {
// Vector3D v2 = p.cross(torque).add(f.projOnto(p));
// velo.add(v2.scale(1.0 / mass));
//
// Vector3D t2 = p.cross(f);
//
// omega += (torque.z + t2.z) / rInertia;
// }

}
