package testbed;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Test extends JFrame {

	private static final long serialVersionUID = 1961133972181932145L;

	public static void main(String arg[]) {
		JFrame frame = new JFrame();
		frame.setLayout(new BorderLayout());

		JPanel gb = new JPanel(new GridBagLayout());
		JButton content = new JButton("Some text");

		JButton filler = new JButton("GG");

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.weightx = 0.1;

		gb.add(content, gbc); // gbc is containing the GridBagConstraints

		gbc.fill = GridBagConstraints.HORIZONTAL;
		//gbc.gridwidth = 1000;
		gbc.weightx = 1;
		System.out.println(gbc.gridx);
		gb.add(filler, gbc);

		frame.add(gb, BorderLayout.CENTER);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}