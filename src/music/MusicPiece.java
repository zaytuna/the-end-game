package music;

import java.io.File;
import java.util.ArrayList;

public class MusicPiece implements Runnable {
	ProgressionGraph pg = ProgressionGraph.createFromFile(new File("Resources\\MusicData\\Working.txt"));
	ProgressionGraph.ChordChange last = pg.nodes[0][1];
	public int key = 60;

	public double tension_harmony = 1;
	public double flow_speed = 1;
	public double flow_volume = 1;

	ArrayList<Rhythm> rhythms = new ArrayList<Rhythm>();
	ArrayList<Theme> themes = new ArrayList<Theme>();

	int repeat = 0;

	public MusicPiece() {
		new Thread(this).start();
	}

	public void run() {
		int counter = 0;
		while (true) {
			counter++;

			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {}

//			flow_speed = (1 + Math.sin(counter / 30D) / 2);
//			flow_volume = 1 + Math.cos(counter / 40D) / 4;
//			System.out.println(flow_speed);

			if (counter % 30 == 0) {
				if (Math.random() > 2.0 / ++repeat) {
					last = last.getNext();
					repeat = 0;
				}

				System.out.println(last.root + "\t" + last.type);
			}
		}
	}
}
