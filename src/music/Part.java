package music;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.Synthesizer;

public abstract class Part {
	int instrumentNum;

	public Part(int n) {
		instrumentNum = n;
	}

	public abstract Note nextNote();

	public abstract boolean isDone();

	public void play(Synthesizer synth, int index) throws Exception {
		new Thread(new PartPlayer(synth, index)).start();
	}

	private class PartPlayer implements Runnable {
		int chanel;
		Synthesizer synth;

		private PartPlayer(Synthesizer synth, int index) {
			this.synth = synth;
			this.chanel = index;
		}

		public void run() {
			Instrument inst = synth.getAvailableInstruments()[instrumentNum];
			MidiChannel chan = synth.getChannels()[chanel];

			synth.loadInstrument(inst);
			chan.programChange(inst.getPatch().getBank(), inst.getPatch().getProgram());
			System.out.println("(" + instrumentNum + ")\t" + inst.getName() + ", " + chanel);

			while (!isDone()) {
				Note n = nextNote();
				if (!n.rest)
					chan.noteOn(n.pitch, n.strength);
				try {
					Thread.sleep(n.durration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (!n.rest)
					chan.noteOff(n.pitch);
			}
			chan.allNotesOff();
		}
	}

	public static class Note {
		boolean rest = false;
		int pitch, durration, strength;

		public Note(int p, int d, int s) {
			pitch = p;
			durration = d;
			strength = s;
			rest = false;
		}

		public Note(int durration) {
			this.durration = durration;
			rest = true;
		}
	}
}
