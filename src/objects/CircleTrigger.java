package objects;

import base.Action;
import base.Player;
import base.Reconstructor;
import base.Trigger;

public class CircleTrigger extends Trigger {
	double x, y, r;

	public CircleTrigger(double a, double b, double c) {
		x = a;
		y = b;
		r = c;
	}

	@Override
	public String pack() {
		StringBuilder built = new StringBuilder();
		for (int i = 0; i < attached.size(); i++)
			built.append("~" + attached.get(i).pack());
		return x + "~" + y + "~" + r + built.toString();
	}

	public static CircleTrigger read(String str) {
		String[] split = str.split("~");
		CircleTrigger ct = new CircleTrigger(Double.parseDouble(split[0]), Double.parseDouble(split[1]),
				Double.parseDouble(split[2]));

		try {

			for (int i = 3; i < split.length; i++) {
// Actions can have \t or : as property delimits. First | is the class name

				String rest = split[i].substring(split[i].indexOf("|") + 1);
				String name = split[i].substring(0, split[i].indexOf("|"));
				Action a;

				a = (Action) Reconstructor.unpack(Class.forName(name), rest, null);

				a.trigger = ct;
				ct.attached.add(a);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return ct;
	}

	@Override
	public boolean condition(Player p) {
		return p.pos.to2DV().sub(x, y).mag2() < r * r;
	}

}
