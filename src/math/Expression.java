package math;

import java.util.ArrayList;

public class Expression {
	double value = Double.NaN;
	int varIndex = -1;

	ArrayList<Expression> components = new ArrayList<Expression>(2);
	Operation op;

	int type = 0;
	String text;

	// 0 = expression
	// 1 = constant
	// 2 = variable

	public Expression(double v) {
		this.value = v;
		op = null;
		type = 1;
		text = "" + v;
	}

	public Expression(Operation o, Expression... a) {
		for (Expression e : a)
			components.add(e);
		type = 0;
		op = o;
		text = toString();
	}

	public Expression(Operation o, ArrayList<Expression> a) {
		components = a;
		type = 0;
		op = o;
	}

	private Expression() {}

	public static Expression createVar(String txt, int i) {
		Expression exp = new Expression();
		exp.varIndex = i;
		exp.type = 2;
		exp.text = txt;
		return exp;
	}

	public double evaluate(double... vars) {
		if (type == 1)
			return value;
		else if (type == 2) {
			if (varIndex >= vars.length)
				return 0;
			return vars[varIndex];
		}

		double[] vals = new double[components.size()];
		for (int i = 0; i < components.size(); i++)
			vals[i] = components.get(i).evaluate(vars);

		value = op.compute(vals);
		return value;
	}

	public String toString() {
		if (type == 0) {
			if (op.position == -1) {
				return formParentheses(components.get(0), this) + " " + op.sign + " "
						+ formParentheses(components.get(1), this);
			}

			StringBuilder build = new StringBuilder();
			for (Expression exp : components)
				build.append(exp.toString() + ", ");

			build.deleteCharAt(build.length() - 1);
			build.deleteCharAt(build.length() - 1);

			if (op.position >= 0) {
				return op.sign + "(" + build.toString() + ")";
			} else if (op.position == -2)
				return "(" + build.toString() + ") " + op.sign;

			return "<herpaderp. operation has no position>";
		} else if (type == 1)
			return value + "";
		else if (type == 2)
			return text;
		// return "<" + varIndex + ">";
		return "\"herpaderp. no type for expression.\"";
	}

	public String treeForm() {
		if (type == 1)
			return value + "";
		else if (type == 2)
			// return text;
			return "<" + varIndex + ">";

		StringBuilder build = new StringBuilder();
		for (Expression exp : components)
			build.append(exp.treeForm());
		return "[" + op + ":  " + build.toString() + "] ";
	}

	private static String formParentheses(Expression child, Expression parent) {
		if ((parent.op != null) && (child.op != null) && parent.op.order < child.op.order)
			return "(" + child.toString() + ")";
		return child.toString();
	}

	public void trySimplify() {
		for (Expression e : components)
			if (e.type == 0)
				e.trySimplify();

		for (Expression e : components) {
			if (e.type != 1)
				return;
		}

		evaluate();
		type = 1;
		components.clear();
		op = null;
	}
}
