package music;

public class Chord {
	public static int NTYPES = 7; // right now, only use major, minor, dominant
	public static final int[][] TYPE_LOOKUP = { // go from types to chords
	{ 0, 4, 7 }, // 0: Major
			{ 0, 3, 7 }, // 1: Minor
			{ 0, 4, 7, 10 }, // 2: Mm7 = Dom7
			{ 0, 4, 7, 11 }, // 3: Maj7
			{ 0, 3, 6, 10 }, // 4: HalfDiminished
			{ 0, 3, 7, 10 }, // 5: Minor7
			{ 0, 3, 6, 9 } // 6: Diminished 7
	};

	int base;// 0-12 bien sur, as a translation from the key.
	int[] offsets;
	double durration;

	protected Chord() {}

	public Chord(int base, double d, int... type) {
		if (type.length < 1)
			throw new IllegalArgumentException("Chord requires at least one note haha");

		this.base = base;
		this.offsets = type;
		this.durration = d;
	}

	public static Chord getRootMajor(int pos, double length) {
		return new Chord(pos, length, 0, 4, 7);
	}

	public static Chord getRootMinor(int pos, double length) {
		return new Chord(pos, length, 0, 3, 7);
	}

	public Chord invertDown() {
		int n = offsets[1 % offsets.length];
		base = (n + base) % 12;
		for (int i = 0; i < offsets.length; i++)
			offsets[i] = (12 + offsets[i] - n) % 12;

		return this;
	}

	public Chord invertUp() {
		int n = offsets[offsets.length - 1];

		base = (n + base) % 12;
		for (int i = 0; i < offsets.length; i++)
			offsets[i] = (12 + offsets[i] - n) % 12;

		return this;
	}

	public int getRandomElement() {
		return offsets[(int) (Math.random() * offsets.length)];
	}
}