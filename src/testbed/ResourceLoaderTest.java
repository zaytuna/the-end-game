package testbed;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ResourceLoaderTest extends JPanel {
	private static final long serialVersionUID = -1509276187603557836L;
	Image img;

	public ResourceLoaderTest() {
		System.out.println();
		URL url = getClass().getClassLoader().getResource("Resources/img.png");

		JOptionPane.showMessageDialog(null, System.getProperty("user.dir"));
		JOptionPane.showMessageDialog(null, getClass().getClassLoader().getResource("img.png"));
		JOptionPane.showMessageDialog(null, new ImageIcon(url));
		img = Toolkit.getDefaultToolkit().createImage(url);
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(img, 0, 0, this);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new ResourceLoaderTest());
		frame.setSize(200, 200);
		frame.setVisible(true);
	}

}
