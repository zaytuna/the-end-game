package base;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import math.Vector3D;
import objects.GameShape;
import objects.HWall;

/**
 * @author Oliver
 *  
 *         It is generally understood that WorldSegments do not overlap. If they
 *         do overlap, then only one set of physics laws will be respected.
 */
public class WorldSegment implements Reconstructable {
	HashMap<String, GameObject> things = new HashMap<String, GameObject>();
	ArrayList<Trigger> trig = new ArrayList<Trigger>();

	public Vector3D gravity = new Vector3D(0, 0.0, 0);
	public Color background = Color.BLACK;
	public double baseFriction = 0;

	Vector3D playerStart = new Vector3D();

	public void add(String s, GameObject g) {
		g.home = this;
		g.name = s;
		things.put(s, g);
	}

	/**
	 * I should actually get rid of this method and put all of the functionality within HWall Constructors.
	 * 
	 * Where is it called? WorldSegment.read()
	 */
	@Deprecated
	public void createWalls() {
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<GameObject> toAdd = new ArrayList<GameObject>();

		for (Entry<String, GameObject> e : things.entrySet()) {
			String str = e.getKey() + "-wall";
			if (e.getValue() instanceof HWall) {
				int counter = 0;
				for (GameShape gs : ((HWall) e.getValue()).create(2)) {
					toAdd.add(gs);
					names.add(str + (counter++));
				}
			}
		}

		for (int i = 0; i < toAdd.size(); i++)
			things.put(names.get(i), toAdd.get(i));
	}

	public static WorldSegment read(String str) {
		String[] lines = str.split("\n");

		WorldSegment ws = new WorldSegment();
		ws.background = new Color(Integer.parseInt(lines[0]));
		ws.gravity = Vector3D.unpack(lines[1]);
		ws.baseFriction = Double.parseDouble(lines[2]);

		int n = 3;

		while (lines[n].startsWith("$")) {
			if (lines[n].startsWith("$start=")) {
				String[] a = lines[n].substring(lines[n].indexOf('=') + 1).split(",");
				double dx, dy, dz;
				dx = Double.parseDouble(a[0]);
				dy = Double.parseDouble(a[1]);
				dz = Double.parseDouble(a[2]);

				ws.playerStart.set(dx, dy, dz);
			}
			n++;
		}

		for (int i = n; i < lines.length; i++) {
			if (lines[i].length() > 0 && lines[i].charAt(0) == '#')
				continue;

			int colen = lines[i].indexOf(":"), dash = lines[i].indexOf("-");
			if (colen > 0) {
				String name = lines[i].substring(0, dash);
				String type = lines[i].substring(dash + 1, colen);
				try {
					Object o = Reconstructor.unpack(Class.forName(type), lines[i].substring(colen + 1), null);
					if (o instanceof GameObject)
						ws.add(name, (GameObject) o);
					if (o instanceof Trigger)
						ws.trig.add((Trigger) o);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}

		// WRONG PLACE!
		//ws.createWalls();

		return ws;
	}

	public static WorldSegment createArena(GameObject... enemies) {
		WorldSegment ws = new WorldSegment();
		ws.background = Color.red;
		return ws;
	}

	@Override
	public String pack() {
		StringBuilder sb = new StringBuilder();
		sb.append(background.getRed() + "\n");
		sb.append(gravity.pack() + "\n");
		sb.append(baseFriction + "\n");

		for (Entry<String, GameObject> e : things.entrySet())
			sb.append(e.getKey() + "-" + e.getValue().getClass().getName() + ":" + e.getValue().pack() + "\n");

		for (Trigger t : trig)
			sb.append("???-" + t.getClass().getName() + ":" + t.pack());

		return sb.toString();
	}

	public GameObject get(String string) {
		return things.get(string);
	}

	public void add(Trigger ct) {
		trig.add(ct);
	}
}
/*
 File structure looks like this:
 
 COLOR IN HEX
 gravX, gravY, gravZ
 Name-ClassName:
 
 */
