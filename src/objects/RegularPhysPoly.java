package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;
import java.util.HashSet;

import math.Vector2D;
import math.Vector3D;
import physics.PolyPObject;
import util.Methods;
import base.Eye;
import base.RenderShard;
import base.TEPanel;

public class RegularPhysPoly extends PolyPObject implements RenderShard {
	Vector3D[] diff;
	Color clr = Methods.randomColor(200);
	double density = 0.5;
	int size;

	public RegularPhysPoly(double r, int nsides, Vector2D v) {
		pos = new Vector3D(v.x, v.y, 0);
		velo = new Vector3D();
		diff = new Vector3D[nsides];
		size = nsides;
		radius = r;
		angle = Math.PI / 6;

		double theta = 2 * Math.PI / size;
		Vector3D toEdge = new Vector3D(1, 0, 0).scale(radius).incrementAngle(angle - theta / 2);

		for (int i = 0; i < size; i++) {
			diff[i] = toEdge.incrementAngle(theta).clone();
		}

		calculateProperties();
		System.out.println("M: " + mass + "\tI:" + rInertia);
	}

	private RegularPhysPoly() {}

	private void calculateProperties() {
		mass = density * 100 * radius * radius;
		rInertia = mass * radius * radius;

		System.out.println("M: " + mass + "\tI:" + rInertia);
	}

	@Override
	public String pack() {
		return size + "\t" + pos.pack() + "\t" + velo.pack() + "\t" + angle + "\t" + omega + "\t" + clr.getRGB();
	}

	public static RegularPhysPoly read(String str) {
		String split[] = str.split("\n");

		RegularPhysPoly ps = new RegularPhysPoly();
		ps.size = Integer.parseInt(split[0]);
		ps.pos = Vector3D.unpack(split[1]);
		ps.velo = Vector3D.unpack(split[2]);
		ps.angle = Double.parseDouble(split[3]);
		ps.omega = Double.parseDouble(split[4]);
		ps.clr = new Color(Integer.parseInt(split[5]), true);
		ps.diff = new Vector3D[ps.size];

		return ps;
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> collector, Eye eye) {
		super.collectRenderShards(collector, eye);
		collector.add(this);
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);
		draw(g, e);
	}

	@Override
	public void specUpdate(HashSet<Integer> keyDown, TEPanel v, double factor) {
		double k = Math.pow(0.97, 1 / factor);
		velo.scale(k);
		omega *= k;

		angle += omega / factor;
		pos.add(velo.clone().scale(1 / factor));

		double theta = 2 * Math.PI / size;
		Vector3D toEdge = new Vector3D(1, 0, 0).scale(radius).incrementAngle(angle - theta / 2);

		for (int i = 0; i < size; i++) {
			diff[i] = toEdge.incrementAngle(theta).clone();
		}
	}

	@Override
	public void draw(Graphics2D g, Eye e) {
		Vector3D temp = pos.clone();

		int[] xpts = new int[size], ypts = new int[size];

		for (int i = 0; i < size; i++) {
			Vector3D v = e.toScreen(temp.set(pos).add(diff[i]));

			if (v == null)
				break;

			xpts[i] = e.psX(v.x);
			ypts[i] = e.psY(v.y);
		}

		g.setColor(e.transform(clr, new Vector3D(0, 0, 1), 0));
		g.fillPolygon(xpts, ypts, size);

		// ** FORCE DRAWING CODE **
		drawForces(g, e, pList2, fList2);

	}

	@Override
	public Vector3D getCenter() {
		return pos;
	}

	@Override
	public Vector3D getNormal() {
		return new Vector3D(0, 0, 1);
	}

	@Override
	public Vector3D[] getVertices() {
		return diff;
	}

}
