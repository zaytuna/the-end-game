package base;

public interface Interactable {
	public abstract void interaction(TEPanel t);
}
