package base;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class TEMain {
	public static Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	public static void main(String[] args) {
		System.out.println(new Color(70,155,50,100).getRGB());
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.setSize(SCREEN);

		TEPanel panel = new TEPanel();
		frame.setContentPane(panel);
		panel.addKeyListener(panel);
		frame.addKeyListener(panel);

		new Thread(panel).start();

		frame.setVisible(true);
	}
}