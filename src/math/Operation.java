package math;

public enum Operation {
	RAND("?", 0, 6), ADD("+", -1, 5), SUB("-", -1, 5), MOD("%", -1, 4), MAX("MAX", 0, 4), MIN("MIN", 0, 4), MULT("*",
			-1, 3), DIV("/", -1, 3), FACT("!", -2, 2), EXP("^", -1, 2), SIN("sin", 1, 1), COS("cos", 1, 1), LN("ln", 1,
			1), LOG("log", 1, 1), SUM("SUM", 0, 2), PROD("PROD", 0, 2), ASIN("asin", 1, 1), ACOS("acos", 1, 1), ATAN(
			"atan", 1, 1), ROUND("round", 1, 0), INT("INT", 1, 0), ABS("abs", 1, 0);

	// it is important that these are listed in descending order of opperations

	String sign;
	int position = 0;// #= num of args = prefix, -1 is infix, -2 is postfix
	int order = 0;

	Operation(String s, int position, int or) {
		sign = s;
		this.position = position;
		this.order = or;
	}

	public double compute(double... nums) {
		if (this == SIN)
			return Math.sin(nums[0]);
		else if (this == COS)
			return Math.cos(nums[0]);
		else if (this == LN)
			return Math.log(nums[0]);
		else if (this == SUM) {
			double s = 0;
			for (double d : nums)
				s += d;
			return s;
		} else if (this == SUB)
			return nums[0] - nums[1];
		else if (this == ADD)
			return nums[0] + nums[1];
		else if (this == MULT)
			return nums[0] * nums[1];
		else if (this == PROD) {
			double p = 1;
			for (double d : nums)
				p *= d;
			return p;
		} else if (this == DIV)
			return nums[0] / nums[1];
		else if (this == EXP)
			return Math.pow(nums[0], nums[1]);
		else if (this == ASIN)
			return Math.asin(nums[0]);
		else if (this == ACOS)
			return Math.acos(nums[0]);
		else if (this == ATAN)
			return Math.atan(nums[0]);
		else if (this == MOD)
			return nums[0] % nums[1];
		else if (this == RAND)
			return Math.random();
		else if (this == ROUND)
			return Math.round(nums[0]);
		else if (this == MIN) {
			double min = Double.MAX_VALUE;
			for (double d : nums)
				if (d < min)
					min = d;
			return min;
		} else if (this == MAX) {
			double max = Double.MIN_VALUE;
			for (double d : nums)
				if (d > max)
					max = d;
			return max;
		} else if (this == INT)
			return (int) nums[0];
		else if (this == LOG)
			return Math.log10(nums[0]);
		else if (this == FACT) {
			int prod = 1;
			for (int i = 1; i < nums[0]; i++)
				prod *= i;
			return prod;
		} else if (this == ABS) {
			return Math.abs(nums[0]);
		}

		return Double.NaN;
	}
}
