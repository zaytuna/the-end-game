package base;

import java.util.ArrayList;

import math.Matrix;
import math.Vector3D;
import util.IOMaster;

public class GeneralFigure {

	public Structure form;
	public Pose pose;
	public Animation animation;

	public GeneralFigure(String structure, String ani) {
		form = Structure.fromFile(structure);
		animation = new Animation.GeneralAnimation(ani, this);
	}

	public GeneralFigure(String structure) {
		form = Structure.fromFile(structure);
		animation = null;
		pose = form.defaultPose.clone();
	}

	public static class Pose {
		public Vector3D[] positions; // from origin
		Structure form;

		Pose(Structure s) {
			this.form = s;
			positions = new Vector3D[s.nPoints];
		}

		public Pose clone() {
			Pose p = new Pose(form);

			for (int i = 0; i < positions.length; i++)
				p.positions[i] = positions[i].clone();

			return p;
		}

		public static Pose fromString(Structure s, String str) {
			Pose p = new Pose(s);
			String[] nums = str.split("\\|");
			for (int i = 0; i < s.nPoints; i++) {
				if (nums[i].length() == 0)
					continue;
				String[] q = nums[i].split(",");
				p.positions[i] = new Vector3D();
				for (int j = 0; j < q.length; j++)
					p.positions[i].set(j, Double.parseDouble(q[j]));
			}
			return p;
		}

		public String export() {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < positions.length; i++) {
				sb.append(positions[i].x + ", " + positions[i].y + ", " + positions[i].z);
				if (i < positions.length - 1)
					sb.append(" | ");
			}
			return sb.toString();
		}

		public static Pose fromString(Structure f, String... vect) {
			Pose p = new Pose(f);
			for (int i = 0; i < f.nPoints; i++) {
				if (vect[i].length() == 0)
					continue;
				String[] q = vect[i].split(",");
				p.positions[i] = new Vector3D();
				for (int j = 0; j < q.length; j++)
					p.positions[i].set(j, Double.parseDouble(q[j]));
			}
			return p;
		}

		public void scale(double d) {
			for (Vector3D v : positions)
				v.scale(d);
		}

		public static Pose meldTransform(Pose p1, Pose p2, double d, Matrix matrix) {
			if (matrix == null) {
				Pose p = new Pose(p1.form);
				for (int i = 0; i < p1.form.nPoints; i++) {
					p.positions[i] = p1.positions[i].clone().add(p2.positions[i].clone().sub(p1.positions[i]).scale(d));
				}
				return p;
			} else {
				Pose p = new Pose(p1.form);
				for (int i = 0; i < p1.form.nPoints; i++) {
					p.positions[i] = matrix.applyTo(p1.positions[i].clone().add(
							p2.positions[i].clone().sub(p1.positions[i]).scale(d)));
				}
				return p;
			}
		}
	}

	public static class Structure {
		public int nPoints;

		// all three arrays of same length, nlines
		public double[] lengths;
		public int[] end1;
		public int[] end2;
		public int head;
		public double headSize;
		public Pose defaultPose = null;

		public String source;

		private Structure() {}

		public static Structure fromFile(String str) {
			Structure struct = new Structure();
			String[] data = IOMaster.getFileContents(str).split("\n");
			struct.nPoints = Integer.parseInt(data[0]);
			String[] fends = data[1].split("\t");
			String[] bends = data[2].split("\t");

			struct.source = str;

			struct.head = Integer.parseInt(data[3]);
			struct.headSize = Double.parseDouble(data[4]);
			struct.defaultPose = Pose.fromString(struct, data[5]);

			if (data.length > 6 && data[6].startsWith("$"))
				struct.defaultPose.scale(Double.parseDouble(data[6].substring(1)));

			struct.end1 = new int[fends.length];
			struct.end2 = new int[bends.length];
			struct.lengths = new double[fends.length];

			for (int i = 0; i < fends.length; i++) {
				struct.end1[i] = Integer.parseInt(fends[i]);
				struct.end2[i] = Integer.parseInt(bends[i]);
				struct.lengths[i] = struct.defaultPose.positions[struct.end1[i]].clone()
						.sub(struct.defaultPose.positions[struct.end2[i]]).magnitude();
			}

			return struct;
		}

		public String export() {
			StringBuilder sb = new StringBuilder();

			sb.append(nPoints + "\n");

			for (int i = 0; i < lengths.length; i++)
				sb.append(lengths[i] + "\t");
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");

			for (int i = 0; i < end1.length; i++)
				sb.append(end1[i] + "\t");
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");

			for (int i = 0; i < end2.length; i++)
				sb.append(end2[i] + "\t");
			sb.deleteCharAt(sb.length() - 1);
			sb.append("\n");

			sb.append(head + "\n");
			sb.append(headSize + "\n");
			sb.append(defaultPose.export() + "\n");

			return sb.toString();
		}
	}

	public static abstract class Animation {
		public GeneralFigure owner;
		public double speed = 1;

		Animation(GeneralFigure s) {
			this.owner = s;
		}

		public String export() {
			return "";
		}

		public abstract int update(double d, double walkAngle);

		public static class Idle extends Animation {
			Idle(GeneralFigure s) {
				super(s);
				owner.pose = owner.form.defaultPose.clone();
			}

			public int update(double d, double a) {
				return 0;
			}
		}

		public static class Walker extends Animation {
			double counter = 0;

			Walker(GeneralFigure s) {
				super(s);
				owner.pose = owner.form.defaultPose.clone();
			}

			public int update(double d, double a) {
				return 0;
				// counter += d.mag();
				// Vector2D l = d.clone().perp().unitize().scale(0.1);
				// pose.positions[0].set(d.clone().unitize().scale(Math.cos(counter
				// / 10) / 10).add(l));
				// pose.positions[1].set(d.clone().unitize().scale(Math.cos(Math.PI
				// + counter / 10) / 10).sub(l));
				// pose.positions[2].set(d.clone().unitize().scale(Math.sin(counter
				// / 10) / 10).add(l));
				// pose.positions[3].set(d.clone().unitize().scale(Math.sin(Math.PI
				// + counter / 10) / 10).sub(l));
			}
		}

		public static class GeneralAnimation extends Animation {
			public ArrayList<Pose> poses = new ArrayList<Pose>();
			public ArrayList<Double> timing = new ArrayList<Double>();
			double counter = 0;
			public boolean loop = true;

			public GeneralAnimation(GeneralFigure s) {
				super(s);
			}

			public GeneralAnimation(String fileName, GeneralFigure s) {
				super(s);

				String str = IOMaster.getFileContents(fileName);
				String[] lines = str.split("\n");

				s.form = Structure.fromFile(lines[0]);

				double scale = 1;

				if (str.contains("|")) {
					for (int i = 1; i < lines.length; i++) {
						if (lines[i].startsWith("#"))
							continue;
						if (lines[i].startsWith("$"))
							scale = Double.parseDouble(lines[i].substring(1));
						else if (lines[i].startsWith("~"))
							speed = Double.parseDouble(lines[i].substring(1));
						else {
							Pose p = Pose.fromString(s.form, lines[i]);
							p.scale(scale);
							poses.add(p);
						}

					}
				} else {
					int counter = 0;
					String[] strings = new String[s.form.nPoints];

					for (int i = 1; i < lines.length; i++) {
						if (lines[i].startsWith("#"))
							continue;
						if (lines[i].startsWith("$"))
							scale = Double.parseDouble(lines[i].substring(1));
						else if (lines[i].startsWith("~"))
							speed = Double.parseDouble(lines[i].substring(1));
						else {
							strings[counter++] = lines[i];

							if (counter == s.form.nPoints) {
								counter = 0;
								Pose p = Pose.fromString(s.form, strings);
								p.scale(scale);
								poses.add(p);
							}
						}

					}
				}
				owner.pose = poses.get(0);
			}

			public String export() {
				StringBuilder sb = new StringBuilder();

				sb.append(owner.form.source + "\n");
				sb.append("# auto-generated blank comment to ensure shit works\n");
				sb.append("~" + speed + "\n");

				for (Pose p : poses)
					sb.append(p.export() + "\n");

				return sb.toString();
			}

			public int update(double d, double a) {
				counter += d * speed;

				// linear pose transform:
				Pose p1 = poses.get(((int) counter) % poses.size()), p2 = poses.get((int) (counter + 1) % poses.size());
				owner.pose = Pose.meldTransform(p1, p2, counter % 1, Matrix.create3DEulerRotMatrix(0, 0, a));

				return (int) counter;
			}
		}
	}
}
