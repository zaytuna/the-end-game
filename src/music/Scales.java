package music;

public class Scales {
	public static int[] NATURAL_MINOR = { 2, 1, 2, 2, 1, 2, 2 };
	public static int[] HARMONIC_MINOR = { 2, 1, 2, 2, 1, 3, 1 };
	public static int[] MAJOR = { 2, 2, 1, 2, 2, 2, 1 };

	public static int[] CHROMATIC = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	public static int[] WHOLE = { 2, 2, 2, 2, 2, 2 };
	public static int[] MINOR_3RDS = { 3, 3, 3, 3 };
	public static int[] MAJOR_3RDS = { 4, 4, 4 };

	public static int[] MIDDLE_EASTERN_MAJOR = { 1, 3, 1, 2, 1, 3, 1 };
	public static int[] MIDDLE_EASTERN_MINOR = { 2, 1, 3, 1, 1, 3, 1 };

	public static int[] JAZZ = { 3, 2, 1, 1, 3, 2 };
}
