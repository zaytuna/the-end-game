package objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;
import java.util.HashSet;

import math.Vector2D;
import math.Vector3D;
import physics.ForceApplier;
import physics.PhysicsObject;
import physics.PolyPObject;
import util.Methods;
import base.Eye;
import base.GameObject;
import base.RenderShard;
import base.TEPanel;

public class CircleWall extends GameObject implements ForceApplier, RenderShard {
	public Color color = Color.GRAY;
	float width = 5;
	boolean operational = true;
	double x, y, r;
	double bounce = 0;

	public CircleWall(double a, double b, double c) {
		x = a;
		y = b;
		r = c;
	}

	@Override
	public void draw(Graphics2D g, Eye e) {
		Vector3D v = new Vector3D(x, y, 0);
		Vector3D q = e.toScreen(v);

		if (q != null) {
			double k = Math.abs(Math.cos(e.beta));
			double w = e.sphereWidth(v, r);

			g.setStroke(operational ? new BasicStroke(width) : new BasicStroke(width, BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_MITER, 10.0f, new float[] { width * 2 }, TEPanel.counter / 1f));
			g.setColor(operational ? e.transform(color, null, 0) : e.transform(Methods.getColor(color, 100), null, 0));
			g.drawOval(e.psX(q.x) - (int) w, e.psY(q.y) - (int) (w * k), (int) (2 * w), (int) (2 * w * k));
			g.setStroke(new BasicStroke(1));
		}
	}

	@Override
	public Vector3D getCenter() {
		return new Vector3D(x, y, 0);
	}

	@Override
	public Vector3D getNormal() {
		return new Vector3D(0, 0, 1);
	}

	@Override
	public void magic(PhysicsObject po) {
		if (operational) {
			Vector2D diff = po.pos.to2DV().sub(new Vector2D(x, y));
			double mag = diff.mag();
			Vector2D n = diff.clone().perp().unitize();

			if (mag > r - po.radius && mag < r + po.radius) {
				diff.unitize();

				if (po instanceof PolyPObject) {
					PhysicsObject.circlePhysics2D((PolyPObject) po, 0.5, x, y, r, diff.to3DV(), bounce, Vector3D.ZERO);
					return;
				}

				else {
					if (mag < r) {
						po.pos.set(diff.clone().scale(r - po.radius));
						po.pos.x += x;
						po.pos.y += y;
						po.norm = diff.to3DV().negate();

					} else if (mag > r) {
						po.pos.set(diff.clone().scale(r + po.radius));
						po.pos.x += x;
						po.pos.y += y;
						po.norm = diff.to3DV();
					}

					Vector3D q = po.velo.projOnto(n).scale(2);
					po.applyForce(diff.to3DV().scale(r), q);
				}
			}
		}
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> collector, Eye eye) {
		super.collectRenderShards(collector, eye);
		collector.add(this);
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);

		draw(g, e);
	}

	@Override
	public void updateU(HashSet<Integer> keyDown, TEPanel v, double factor) {
		super.updateU(keyDown, v, factor);
	}

	public String pack() {
		return color.getRGB() + "\t" + width + "\t" + (operational ? 't' : 'f') + "\t" + x + "\t" + y + "\t" + r + "\t"
				+ bounce;
	}

	public static CircleWall read(String str) {
		String[] split = str.split("\t");
		CircleWall cw = new CircleWall(Double.parseDouble(split[3]), Double.parseDouble(split[4]),
				Double.parseDouble(split[5]));
		cw.color = new Color(Integer.parseInt(split[0]));
		cw.width = Float.parseFloat(split[1]);
		cw.operational = split[2].startsWith("t");
		cw.bounce = Double.parseDouble(split[6]);
		return cw;
	}
}
