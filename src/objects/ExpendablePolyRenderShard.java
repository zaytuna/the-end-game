package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import math.Vector3D;
import base.Eye;
import base.RenderShard;

public class ExpendablePolyRenderShard implements RenderShard {
	int[] xpts, ypts;

	Vector3D center, normal;
	Color c;
	Stroke k;

	boolean filled = true;

	public ExpendablePolyRenderShard(Color c, Stroke k, int[] x, int[] y, Vector3D q, Vector3D n, boolean f) {
		this.xpts = x;
		this.ypts = y;
		this.k = k;
		this.c = c;
		this.center = q;
		this.normal = n;
		this.filled = f;
	}

	public ExpendablePolyRenderShard(int x1, int[] x, int[] y, Vector3D q, Vector3D n) {
		this(null, null, x, y, q, n, true);
	}

	@Override
	public void draw(Graphics2D g, Eye e) {
		if (c != null)
			g.setColor(c);
		if (k != null)
			g.setStroke(k);

		if (filled)
			g.fillPolygon(xpts, ypts, xpts.length);
		else g.drawPolygon(xpts, ypts, xpts.length);
	}

	@Override
	public Vector3D getCenter() {
		return center;
	}
	
	@Override
	public Vector3D getNormal() {
		return normal;
	}
}
