package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;
import java.util.HashSet;

import math.Matrix;
import math.Vector3D;
import physics.PolyPObject;
import base.Eye;
import base.RenderShard;
import base.TEPanel;

public class Box extends PolyPObject {

	// FRONT, BACK, TOP, BOTTOM, LEFT, RIGHT
	public static final int[][] FACES = { { 0, 1, 2, 3 }, { 4, 5, 6, 7 }, { 0, 1, 5, 4 }, { 2, 3, 7, 6 },
			{ 0, 3, 7, 4 }, { 1, 2, 6, 5 } };
	public static final Vector3D[] NORMS = { new Vector3D(0, 0, 1), new Vector3D(0, 0, -1), new Vector3D(0, -1, 0),
			new Vector3D(0, 1, 0), new Vector3D(-1, 0, 0), new Vector3D(1, 0, 0) };

	// 3D rotation variables, for a new era of better physics
	double beta = 0, gamma = 0, dbeta = 0, dgamma = 0;

	Color clr = new Color(200, 100, 200, 150);
	double density = 0.5;
	private double width, height, depth;

	private Vector3D[] diff = new Vector3D[8];

	private Matrix rotator;

	public Box(double x, double y, double z, Vector3D p) {
		pos = p.clone();
		velo = new Vector3D();
		this.width = x;
		this.height = y;
		this.depth = z;
		radius = Math.sqrt(x * x + y * y) / 2;

		this.density = 0.5;

		diff[0] = new Vector3D(-x / 2, -y / 2, z / 2);
		diff[1] = new Vector3D(x / 2, -y / 2, z / 2);
		diff[2] = new Vector3D(x / 2, y / 2, z / 2);
		diff[3] = new Vector3D(-x / 2, y / 2, z / 2);
		diff[4] = new Vector3D(-x / 2, -y / 2, -z / 2);
		diff[5] = new Vector3D(x / 2, -y / 2, -z / 2);
		diff[6] = new Vector3D(x / 2, y / 2, -z / 2);
		diff[7] = new Vector3D(-x / 2, y / 2, -z / 2);

		rotator = Matrix.create3DEulerRotMatrix(angle, beta, gamma);
		calculateProperties();
	}

	private Box() {}

	private void calculateProperties() {
		mass = density * 100 * width * height * depth;
		rInertia = 1 / 12D * mass * (width * width + depth * depth);
		System.out.println("M: " + mass + "\tI:" + rInertia);
	}

	@Override
	public String pack() {
		return width + "\t" + height + "\t" + depth + "\t" + pos.pack() + "\t" + velo.pack() + "\t" + angle + "\t"
				+ omega + "\t" + clr.getRGB() + "\t" + density;
	}

	public static Box read(String str) {
		String split[] = str.split("\n");

		Box b = new Box();
		b.width = Double.parseDouble(split[0]);
		b.height = Double.parseDouble(split[1]);
		b.depth = Double.parseDouble(split[2]);

		b.pos = Vector3D.unpack(split[3]);
		b.velo = Vector3D.unpack(split[4]);

		b.angle = Double.parseDouble(split[5]);
		b.omega = Double.parseDouble(split[6]);

		b.clr = new Color(Integer.parseInt(split[7]), true);

		b.density = Double.parseDouble(split[8]);

		return b;
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> collector, Eye eye) {
		super.collectRenderShards(collector, eye);

		int[] xpts = new int[8], ypts = new int[8];

		for (int i = 0; i < 8; i++) {
			Vector3D v = eye.toScreen(pos.clone().add(rotator.applyTo(diff[i])));
			if (v == null) {
				xpts[i] = -1;
				ypts[i] = -1;
				continue;
			}

			xpts[i] = eye.psX(v.x);
			ypts[i] = eye.psY(v.y);
		}

		for (int Q = 0; Q < 6; Q++) {
			if (xpts[Q] == -1 && ypts[Q] == -1)
				continue;
			Vector3D n = rotator.applyTo(NORMS[Q]);
			collector.add(new ExpendablePolyRenderShard(eye.transform(clr, n, 0), null, new int[] { xpts[FACES[Q][0]], xpts[FACES[Q][1]],
					xpts[FACES[Q][2]], xpts[FACES[Q][3]] }, new int[] { ypts[FACES[Q][0]], ypts[FACES[Q][1]],
					ypts[FACES[Q][2]], ypts[FACES[Q][3]] }, n.clone().scale(lTo(Q)).add(pos), n.clone(), true));
		}
	}

	private double lTo(int face) {
		switch (face) {
		case 0:
		case 1:
			return depth / 2;
		case 2:
		case 3:
			return height / 2;
		case 4:
		case 5:
			return width / 2;
		}
		return 0;
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);
		fakeBackupDraw(g, e);
	}

	@Override
	public void specUpdate(HashSet<Integer> keyDown, TEPanel v, double factor) {
		angle += omega / factor;
		pos.add(velo.clone().scale(1 / factor));

		
		double k = Math.pow(1/(1+home.baseFriction/20), 1 / factor);
		velo.scale(k);
		omega *= k;

		rotator = Matrix.create3DEulerRotMatrix(angle, beta, gamma);
	}

	@Override
	public Vector3D[] getVertices() {
		return new Vector3D[] { rotator.applyTo(diff[0].clone().add(diff[4]).scale(0.5)),
				rotator.applyTo(diff[1].clone().add(diff[5]).scale(0.5)),
				rotator.applyTo(diff[2].clone().add(diff[6]).scale(0.5)),
				rotator.applyTo(diff[3].clone().add(diff[7]).scale(0.5)) };
	}

}
