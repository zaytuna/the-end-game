package base;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.HashSet;

import objects.ExpendableCircleRenderShard;
import objects.ExpendableLineRenderShard;
import physics.PhysicsObject;
import math.Vector2D;
import math.Vector3D;

public class Player extends PhysicsObject {
	public static final int UP = 0, LEFT = 1, DOWN = 2, RIGHT = 3, JUMP = 4, SWITCH = 5;
	int[] keyBindings = { KeyEvent.VK_W, KeyEvent.VK_A, KeyEvent.VK_S, KeyEvent.VK_D, KeyEvent.VK_SPACE, KeyEvent.VK_Q };

// public StickFigure display = new
// StickFigure("Resources/Structures/13PTFIG.struct",
// "Resources/Animations/predef2.anim");
	public GeneralFigure display = new GeneralFigure("Resources/Structures/13PTFIG.struct",
			"Resources/Animations/predef.anim");

	Vector2D walkVelo = new Vector2D(0, 0);
	public double walkAngle = 0;

	Player() {
		pos = new Vector3D();
		velo = new Vector3D();
		radius = 0.20;
		mass = 10;
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);
		Vector3D temp = new Vector3D();

		int[] x = new int[display.form.nPoints], y = new int[display.form.nPoints];
		for (int i = 0; i < display.form.nPoints; i++) {
			Vector3D v = e.toScreen(temp.set(display.pose.positions[i]).add(pos));
			if (v == null)
				break;
			x[i] = e.psX(v.x);
			y[i] = e.psY(v.y);
		}

		g.setColor(Color.GRAY);
		for (int i = 0; i < display.form.lengths.length; i++)
			g.drawLine(x[display.form.end1[i]], y[display.form.end1[i]], x[display.form.end2[i]],
					y[display.form.end2[i]]);

		int w = e.sphereWidth(temp.set(display.pose.positions[display.form.head]).add(pos), display.form.headSize);
		g.fillOval(x[display.form.head] - w, y[display.form.head] - w, w * 2, w * 2);
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> collector, Eye e) {
		super.collectRenderShards(collector, e);
		Vector3D temp = new Vector3D();

		int[] x = new int[display.form.nPoints], y = new int[display.form.nPoints];
		for (int i = 0; i < display.form.nPoints; i++) {
			Vector3D v = e.toScreen(temp.set(display.pose.positions[i]).add(pos));
			if (v == null)
				break;
			x[i] = e.psX(v.x);
			y[i] = e.psY(v.y);
		}

		for (int i = 0; i < display.form.lengths.length; i++) {
			collector.add(new ExpendableLineRenderShard(Color.GRAY, null, x[display.form.end1[i]],
					y[display.form.end1[i]], x[display.form.end2[i]], y[display.form.end2[i]],
					display.pose.positions[display.form.end1[i]].clone().add(pos), new Vector3D(0,0,1)));
		}

		int w = e.sphereWidth(temp.set(display.pose.positions[display.form.head]).add(pos), display.form.headSize);
		collector.add(new ExpendableCircleRenderShard(Color.GRAY, null, x[display.form.head], y[display.form.head], w,
				display.pose.positions[display.form.head].clone().add(pos), new Vector3D(0,0,1), true));
	}

	@Override
	public void updateU(HashSet<Integer> keyDown, TEPanel v, double f) {
		super.updateU(keyDown, v, f);
		
		Vector2D m = new Vector2D(0, 0);
		Vector2D up = v.view.pickZProj(new Vector2D(0, -1)).to2DV().unitize(), right = v.view
				.pickZProj(new Vector2D(1, 0)).to2DV().unitize();
		if (keyDown.contains(keyBindings[LEFT]))
			m.minus(right);
		if (keyDown.contains(keyBindings[RIGHT]))
			m.add(right);
		if (keyDown.contains(keyBindings[UP]))
			m.add(up);
		if (keyDown.contains(keyBindings[DOWN]))
			m.minus(up);

		m.unitize();
		m.scale(0.05);

		double aTo = walkAngle;

		// this is to normalize the walkToAngle to its [0, 2pi) range
		if (m.mag2() > 0) {
			aTo = Math.atan2(m.y, m.x) - Math.PI;

			if (Math.abs(walkAngle - aTo) > Math.PI)
				if (walkAngle < aTo)
					aTo -= Math.PI * 2;
				else aTo += Math.PI * 2;

			walkAngle %= 2 * Math.PI;
		}

		walkAngle += (aTo - walkAngle) / 10;

		//if (norm != null || home.gravity.mag2() < 0.0000001)
			velo.add(new Vector3D(m.x / f, m.y / f, velo.z).sub(velo).scale(friction/3));

		display.animation.update(m.mag() / f, walkAngle);

		if (norm != null && keyDown.contains(keyBindings[JUMP]))
			velo.add(norm.clone().scale(0.09));

		pos.add(velo.clone().scale(1));

		norm = null;
		friction = home.baseFriction;
	}

	@Override
	public String pack() {
		// TODO Add when we figure out how to do this
		return null;
	}

	@Override
	public void applyForce(Vector3D p, Vector3D f) {
		velo.add(f.clone().scale(1.0/mass));
	}
}
