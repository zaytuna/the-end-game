package music;

import java.io.File;

import util.Methods;

public class ProgressionGraph {
	ChordChange[][] nodes;

	private ProgressionGraph() {
		nodes = new ChordChange[12][Chord.NTYPES];
	}

	public static ProgressionGraph createFromFile(File f) {
		String[] lines = Methods.getFileContents(f).split("\n");

		ProgressionGraph pg = new ProgressionGraph();

		for (int i = 1; i < lines.length; i++) {
			if (lines[i].length() < 2 || lines[i].startsWith("#"))
				continue;

			String[] pieces = lines[i].split("\t");
			int[] co = getCoordinates(pieces[0]);

			int num = pieces.length - 1;

			int[] roots = new int[num];
			int[] types = new int[num];
			double[] weights = new double[num];

			for (int j = 1; j < num + 1; j++) {
				int[] coj = getCoordinates(pieces[j]);
				roots[j - 1] = coj[0];
				types[j - 1] = coj[1];
				weights[j - 1] = 7.0 / (double) num;
			}

			pg.nodes[co[0]][co[1]] = pg.new ChordChange(co[0], co[1], roots, types, weights);
		}

		   
		for (int i = 0; i < 12; i++)
			for (int j = 0; j < Chord.NTYPES; j++) {
				if (pg.nodes[i][j] == null)
					pg.nodes[i][j] = pg.createReset(i, j, i, 0);
			}

		return pg;
	}

	public static int[] getCoordinates(String name) {
		char s = name.charAt(name.length() - 1);
		int symbol = -1;

		switch (s) {
		case 'M':
			symbol = 0;
			break;
		case 'm':
			symbol = 1;
			break;
		case 'D':
			symbol = 2;
			break;
		case 'J':
			symbol = 3;
			break;
		case 'H':
			symbol = 4;
			break;
		case 'n':
			symbol = 5;
			break;
		case '-':
			symbol = 6;
			break;
		}

		return new int[] { Integer.parseInt(name.substring(0, name.length() - 1)), symbol };
	}

	public static ProgressionGraph createComplete() {
		ProgressionGraph pg = new ProgressionGraph();

		for (int i = 0; i < 12; i++)
			for (int j = 0; j < Chord.NTYPES; j++) {
				pg.nodes[i][j] = pg.new ChordChange(i, j, countLong(12, Chord.NTYPES), countWide(12, Chord.NTYPES),
						fillArray(12 * Chord.NTYPES, 1.0));
			}

		return pg;
	}

	public ChordChange createReset(int a, int b, int r, int t) {
		return new ChordChange(a, b, new int[] { r }, new int[] { t }, new double[] { 1.01 });
	}

	public class ChordChange {
		public final int root, type;

		final int options;
		private final int[] childroots; // 0-12
		private final int[] childtypes;
		private final double[] childweights; // for net training

		final double tot;

		public ChordChange(int a, int b, int[] r, int[] t, double[] w) {
			this.root = a;
			this.type = b;

			this.options = r.length;
			this.childroots = r;
			this.childtypes = t;
			this.childweights = w;

			int q = 0;
			for (int i = 0; i < options; i++)
				q += w[i];
			tot = q;
		}

		public ChordChange getNext() {
			double ticket = Math.random() * tot;
			int q = 0;
			for (int i = 0; i < options; i++) {
				q += childweights[i];
				if (ticket < q)
					return nodes[childroots[i]][childtypes[i]];
			}
			throw new IllegalStateException("No weight in any of the outputs! Dead music :(");
		}
	}

	private static double[] fillArray(int n, double d) {
		double[] q = new double[n];
		for (int i = 0; i < n; i++)
			q[i] = d;
		return q;
	}

	private static int[] countLong(int l, int w) {
		int[] x = new int[l * w];
		for (int i = 0; i < l; i++)
			for (int j = 0; j < w; j++)
				x[i + l * j] = i;
		return x;
	}

	private static int[] countWide(int l, int w) {
		int[] x = new int[l * w];
		for (int i = 0; i < l; i++)
			for (int j = 0; j < w; j++)
				x[i + l * j] = j;
		return x;
	}
}
