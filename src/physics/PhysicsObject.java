package physics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;

import base.Eye;
import base.GameObject;
import math.Vector2D;
import math.Vector3D;
import util.Methods;

public abstract class PhysicsObject extends GameObject {
	public Vector3D pos;
	public Vector3D velo;
	public Vector3D norm;

	public double mass = 20, rInertia = 10;
	public double radius = 0.3, friction = 0;

	double elasticity = 0.5;

	public abstract void applyForce(Vector3D p, Vector3D f);
	
	public static void circlePhysics2D(PolyPObject po, double friction, double x, double y, double r, Vector3D norm, double bounce, Vector3D vel) {
		int normside = (int)Math.signum(po.pos.to2DV().sub(x,y).mag2() - r*r);

		PolyPObject ppo = ((PolyPObject) po);
		Vector3D[] vertices = ppo.getVertices();

		for (int i = 0; i < vertices.length; i++) {
			int s = (int)Math.signum(po.pos.to2DV().add(vertices[i]).sub(x,y).mag2() - r*r);

			if (s * normside < 0) {
				Vector3D parvv = ppo.getVVelo(i).sub(vel);
				Vector3D projvv = parvv.projOnto(norm).negate();
				parvv.add(projvv);

				po.norm = norm.clone();
				ppo.applyForce(vertices[i].clone(), projvv);
				po.pos.add(vertices[i].clone().add(norm).projOnto(norm).negate());
				po.friction = friction;

				Vector3D fV = parvv.scale(-friction);
				ppo.applyForce(vertices[i].clone(), fV);
			}
		}

	}

	public static void linePhysics2D(PolyPObject po, double friction, double x1, double y1, double x2, double y2,
			Vector3D vel) {
		Vector2D d = new Vector2D(x2 - x1, y2 - y1), pp = po.pos.to2DV().minus(new Vector2D(x1, y1));
		pp.minus(pp.projOnto(d));

		linePhysics2D(po, friction, x1, y1, x2, y2, pp.to3DV(), 0, vel);
	}

	public static void linePhysics2D(PolyPObject po, double friction, double x1, double y1, double x2, double y2,
			Vector3D norm, double bounce, Vector3D vel) {
		int normside = side(po.pos.x, po.pos.y, x1, y1, x2, y2);

		PolyPObject ppo = ((PolyPObject) po);
		Vector3D[] vertices = ppo.getVertices();

		for (int i = 0; i < vertices.length; i++) {
			int s = side(po.pos.x + vertices[i].x, po.pos.y + vertices[i].y, x1, y1, x2, y2);
			Vector2D v1 = vertices[i].to2DV().add(po.pos);

			if (s * normside < 0 && ptIn(v1, x1, y1, x2, y2)) {
				Vector3D parvv = ppo.getVVelo(i).sub(vel);
				Vector3D projvv = parvv.projOnto(norm).negate();
				parvv.add(projvv);

				po.norm = norm.clone();
				ppo.applyForce(vertices[i].clone(), projvv);
				po.pos.add(vertices[i].clone().add(norm).projOnto(norm).negate());
				po.friction = friction;

				Vector3D fV = parvv.scale(-friction);
				ppo.applyForce(vertices[i].clone(), fV);
				// System.out.println(projvv);
			}
		}

// for (int i = 0; i < vertices.length; i++) {
// Vector2D v1 = vertices[i].to2DV(), v2 = vertices[(i + 1) %
// vertices.length].to2DV(), v3 = new Vector2D(
// x1-po.pos.x, y1-po.pos.y), v4 = new Vector2D(x2-po.pos.x, y2-po.pos.y);
// // System.out.println(v1 + "\t" + v2 + "\t" + v3 + "\t" + v4);
// Vector2D b = Methods.segmentIntersect(v1, v2, v3, v4);
//
// if (b != null) {
// System.out.println(x1+","+y1);
// //apply (v1,v2)perp
// po.norm = norm.clone();
// po.applyForce(b.to3DV(), v2.sub(v1).perp().to3DV().normalize().scale(0.1));
// }
// }
	}

	public static int side(Vector2D v, Vector2D v1, Vector2D v2) {
		return side(v.x, v.y, v1.x, v1.y, v2.x, v2.y);
	}

	public static int side(double x, double y, double x1, double y1, double x2, double y2) {
		return (int) Math.signum((x2 - x1) * (y - y1) - (x - x1) * (y2 - y1));
	}

	public void drawForces(Graphics2D g, Eye e, List<Vector3D> pList2, List<Vector3D> fList2) {
		g.setStroke(new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g.setColor(Color.WHITE);
		Vector3D temp = new Vector3D();

		for (int i = 0; i < pList2.size(); i++) {

			Vector3D q = e.toScreen(temp.set(pos).add(pList2.get(i)));
			if (q == null)
				break;

			int x = e.psX(q.x), y = e.psY(q.y);
			g.fillOval(x - 3, y - 3, 6, 6);

			Vector3D qq = e.toScreen(temp.set(pos).add(pList2.get(i)).add(fList2.get(i).scale(10)));
			if (qq == null)
				break;

			Methods.drawArrow(g, x, y, e.psX(qq.x), e.psY(qq.y));
		}

		g.setStroke(new BasicStroke(1));
	}

	public static boolean ptIn(Vector2D v, double x1, double y1, double x2, double y2) {
		Vector2D d = new Vector2D(x2 - x1, y2 - y1), pp = v.minus(new Vector2D(x1, y1));
		Vector2D prd = pp.projOnto(d);

		return (prd.mag() <= d.mag() && prd.dot(d) > 0);
	}
}
