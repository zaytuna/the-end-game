package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;

import math.Vector3D;
import util.Methods;
import base.Eye;
import base.GameObject;
import base.RenderShard;

public class GameShape extends GameObject implements RenderShard {
	ArrayList<Vector3D> vertices = new ArrayList<Vector3D>();
	Vector3D normal;
	Vector3D center;
	Color color;

	boolean borders = true;

	public GameShape(double... stuff) {
		this(Methods.randomColor(100), true, stuff);
	}

	public GameShape(Color c, boolean b, double... coordinates) {
		for (int i = 0; i + 2 < coordinates.length; i += 3) {
			Vector3D v = new Vector3D(coordinates[i], coordinates[i + 1], coordinates[i + 2]);
			vertices.add(v);
		}
		color = c;
		borders = b;
		updateCenter();
		calculateNorm();
	}
	
	public GameShape setBorder(boolean b) {
		borders = b;
		return this;
	}

	public void calculateNorm() {
		Vector3D totNorm = new Vector3D();

		for (int i = 0; i < vertices.size(); i++) {
			Vector3D cross = vertices.get(i).clone().sub(center)
					.cross(vertices.get((i + 1) % vertices.size()).clone().sub(center)).abs();
			totNorm.add(cross);
		}
		normal = totNorm;
	}

	public void updateCenter() {
		center = new Vector3D();
		for (Vector3D v : vertices)
			center.add(v);

		center.scale(1D / vertices.size());
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);
		draw(g, e);
	}

	@Override
	public void draw(Graphics2D g, Eye e) {
		int[] xpoints = new int[vertices.size()];
		int[] ypoints = new int[vertices.size()];

		for (int i = 0; i < vertices.size(); i++) {
			Vector3D proj = e.toScreen(vertices.get(i));
			if (proj == null)
				return;
			xpoints[i] = e.psX(proj.x);
			ypoints[i] = e.psY(proj.y);
		}

		g.setColor(e.transform(color, normal, 0));
		g.fillPolygon(xpoints, ypoints, vertices.size());

		if (borders) {
			g.setColor(e.transform(Methods.getColor(color.brighter(), 255), normal, 0));
			g.drawPolygon(xpoints, ypoints, xpoints.length);
		}
	}

	@Override
	public Vector3D getCenter() {
		return center;
	}
	
	@Override
	public Vector3D getNormal() {
		return normal;
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> c, Eye e) {
		super.collectRenderShards(c, e);
		c.add(this);
	}

	public String pack() {
		StringBuilder sb = new StringBuilder();
		sb.append(color.getRGB());
		for (Vector3D v : vertices)
			sb.append('\t' + v.pack());

		return sb.toString();
	}

	public static GameShape read(String str) {
		String[] split = str.split("\t");
		double[] v = new double[3 * (split.length - 1)];

		for (int i = 0; i < v.length; i += 3) {
			String[] mini = split[i / 3 + 1].split(",");
			v[i] = Double.parseDouble(mini[0]);
			v[i + 1] = Double.parseDouble(mini[1]);
			v[i + 2] = Double.parseDouble(mini[2]);
		}

		return new GameShape(new Color(Integer.parseInt(split[0]), true), true, v);
	}

}
