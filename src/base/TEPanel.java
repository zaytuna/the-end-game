
package base;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import javax.swing.JPanel;

import math.Vector2D;
import math.Vector3D;
import objects.BlockingMessage;
import objects.CircleTrigger;
import objects.Key;
import objects.RegularPhysPoly;
import physics.ForceApplier;
import physics.PhysicsObject;
import util.IOMaster;
import util.Methods;

public class TEPanel extends JPanel implements Runnable, KeyListener {
	private static final long serialVersionUID = -49784646977583567L;

	public static int counter;

	public ArrayList<WorldSegment> loaded = new ArrayList<WorldSegment>();
	public ArrayList<Overlay> overlays = new ArrayList<Overlay>();
	public WorldSegment current;
	public Player player;
	public Eye view;

	private HashSet<Integer> keys = new HashSet<Integer>();
	private ArrayList<RenderShard> renderList = new ArrayList<RenderShard>();

	private int fps, minFPS, updateFPS;
	private long lastTime = System.currentTimeMillis(), ltUpdate = System.currentTimeMillis();

	private boolean drawn = false;
	boolean orthoTo = false;

	public double slowmo = 1;

	public TEPanel() {
		setDoubleBuffered(true);
		view = new Eye(TEMain.SCREEN);

		view.ortho = (orthoTo ? 1 : 0);

		WorldSegment ws = WorldSegment.read(IOMaster.getFileContents("Resources/Maps/Level1.ws"));
		// WorldSegment ws =
		// WorldSegment.read(IOMaster.getFileContents("Resources/Maps/PhysicsTest.ws"));
		player = new Player();
		player.pos.set(ws.playerStart);
		view.focus.set(ws.playerStart);

		ws.add("Plr", player);

		// ws.add("BOX!", new Box(2, 0.5, 1, new Vector3D(15, 2, 0.5)));
		// ws.add("BLOCK", new RegularPhysPoly(0.9, 8, new Vector2D(0, 2)));
		// ws.add("BLOCK1", new RegularPhysPoly(0.8, 10, new Vector2D(-1, -2)));
		ws.add("BLOCK2", new RegularPhysPoly(0.4, 4, new Vector2D(1, -1)));

		CircleTrigger ct = new CircleTrigger(8, 8, 2);
		ct.attached.add(new BlockingMessage(ct, "Hello!"));
		ws.add(ct);

		Key key = new Key(15, 4, 0, Color.red);
		ws.add("KEY", key);

		setBackground(ws.background);
		current = ws;
		loaded.add(ws);
	}

	public HashSet<Integer> getKeys() {
		return keys;
	}

	public void run() {
		while (true) {
			Color bestColor = current.background;
			if (!bestColor.equals(getBackground()))
				setBackground(Methods.colorMeld(getBackground(), bestColor, 0.05));

			if (drawn) {
				long t = System.currentTimeMillis();
				updateFPS = (int) (1000f / (t - ltUpdate));
				double factor = 10D / ( (t - ltUpdate) * slowmo);
				ltUpdate = t;

				if (counter % 100 == 0)
					minFPS = fps;

				if (slowmo > 0) {
					counter++;

					double diff = 0.0010;

					if (keys.contains(KeyEvent.VK_E))
						view.da += diff / factor;
					if (keys.contains(KeyEvent.VK_Q))
						view.da -= diff / factor;
					if (keys.contains(KeyEvent.VK_X))
						view.db += diff / factor;

					if (keys.contains(KeyEvent.VK_Z))
						view.db -= diff / factor;
					if (keys.contains(KeyEvent.VK_M))
						view.ds += diff / factor;
					if (keys.contains(KeyEvent.VK_N))
						view.ds -= diff / factor;
					

					if (GlobalOptions.sort) {
						ArrayList<RenderShard> buffer = new ArrayList<RenderShard>();

						for (int i = 0; i < loaded.size(); i++)
							for (GameObject o : loaded.get(i).things.values())
								if (o.visible)
									o.collectRenderShards(buffer, view);

						 Collections.sort(buffer, view);

						renderList = buffer;
					}

					view.ortho += ( (orthoTo ? 1 : 0) - view.ortho) / 8;

					updateAll(factor);
					view.update(factor);

					view.focusApproach(player.pos.to2DV(), factor);
				} else {
					overlayUpdate();
				}

				drawn = false;
				repaint();
			}

			try {
				Thread.sleep(10);
			} catch (Exception e) {}

		}
	}

	public void updateAll(double factor) {
		for (int i = 0; i < loaded.size(); i++) {

			for (GameObject o : loaded.get(i).things.values()) {
				if (o instanceof PhysicsObject) {
					PhysicsObject po = ((PhysicsObject) o);
					po.applyForce(Vector3D.ZERO, loaded.get(i).gravity.clone().scale(po.mass));

					for (String s : loaded.get(i).things.keySet())
						if (loaded.get(i).things.get(s) instanceof ForceApplier)
							((ForceApplier) loaded.get(i).things.get(s)).magic((PhysicsObject) o);
				}
			}

			HashSet<String> remove = new HashSet<String>();

			for (String n : loaded.get(i).things.keySet()) {
				GameObject o = loaded.get(i).get(n);
				if (o.flag == 1)
					remove.add(n);
				o.updateU(keys, this, factor);
			}

			for (String n : remove)
				loaded.get(i).things.remove(n);

			for (Trigger t : loaded.get(i).trig)
				if (t.isActive(player))
					t.fire(this);
		}
		overlayUpdate();
	}

	private void overlayUpdate() {
		for (int i = 0; i < overlays.size(); i++) {
			overlays.get(i).update(this);
			if (overlays.get(i).isDead(this)) {
				overlays.remove(i);
				i--;
			}
		}
	}

	public void paintComponent(Graphics grr) {
		super.paintComponent(grr);

		long t = System.currentTimeMillis();

		fps = (int) (1000f / (t - lastTime));
		if (fps < minFPS)
			minFPS = fps;
		lastTime = t;

		Graphics2D g = (Graphics2D) grr;

		if (GlobalOptions.antialias)
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		view.synchronize();
		if (GlobalOptions.sort) {
			int size = renderList.size();

			for (int i = 0; i < size; i++) {
				renderList.get(i).draw(g, view);
				// Vector2D v = view.toScreen(renderList.get(i).getCenter());
				// if (v != null) {
				// g.setColor(Color.WHITE);
				// g.fillOval(view.psX(v.x) - 5, view.psY(v.y) - 5, 10, 10);
				// g.setColor(Color.RED);
				// g.drawString("" + (size - i), view.psX(v.x), view.psY(v.y));
				// }
			}
		} else
			for (int i = 0; i < loaded.size(); i++) {
				for (GameObject r : loaded.get(i).things.values())
					if (r.visible)
						r.backupDrawU(g, view);
			}

		g.setColor(Methods.getOppositeColor(getBackground()));
		g.drawString(fps + ", " + minFPS + ", " + updateFPS, 20, 40);
		drawn = true;

		for (int i = 0; i < overlays.size(); i++)
			overlays.get(i).draw(g, this);
	}

	public void keyPressed(KeyEvent evt) {
		keys.add(evt.getKeyCode());

		if (evt.getKeyCode() == KeyEvent.VK_F1) {
			GlobalOptions.sort = !GlobalOptions.sort;
		} else if (evt.getKeyCode() == KeyEvent.VK_F2)
			GlobalOptions.ovals = !GlobalOptions.ovals;
		else if (evt.getKeyCode() == KeyEvent.VK_F3)
			orthoTo = !orthoTo;
		else if (evt.getKeyCode() == KeyEvent.VK_F4)
			GlobalOptions.antialias = !GlobalOptions.antialias;
	}

	public void keyReleased(KeyEvent evt) {
		keys.remove(evt.getKeyCode());
	}

	public void keyTyped(KeyEvent arg0) {}
}
