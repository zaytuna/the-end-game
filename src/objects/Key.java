package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Collection;
import java.util.HashSet;

import math.Vector2D;
import math.Vector3D;
import base.Eye;
import base.InventoryObject;
import base.RenderShard;
import base.TEPanel;

public class Key extends InventoryObject implements RenderShard {
	Color clr = Color.RED;
	private double rotate = 0;

	int[] keys = new int[7];

	public Key(double x, double y, double z, Color c) {
		pos = new Vector3D(x, y, z);
		clr = c;

		for (int i = 0; i < keys.length; i++)
			keys[i] = (int) (((Math.random() * 10) + 1) * Math.signum(Math.random() - 0.5));
	}

	private Key() {}

	@Override
	public String pack() {
		StringBuilder keyStr = new StringBuilder();

		for (int i = 0; i < keys.length; i++)
			keyStr.append(keys[i] + ",");

		keyStr.deleteCharAt(keyStr.length() - 1);

		return pos.pack() + "\t" + clr.getRGB() + "\t" + keyStr.toString();
	}

	public static Key read(String str) {
		String[] tabs = str.split("\t");

		Key key = new Key();
		key.pos = Vector3D.unpack(tabs[0]);
		key.clr = new Color(Integer.parseInt(tabs[1]), true);

		String[] split2 = tabs[2].split(",");

		key.keys = new int[split2.length];
		for (int i = 0; i < split2.length; i++)
			key.keys[i] = Integer.parseInt(split2[i]);

		return key;
	}

	@Override
	public Image getIcon() {
		return null;
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> collector, Eye eye) {
		super.collectRenderShards(collector, eye);
		collector.add(this);
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);
		draw(g, e);
	}

	@Override
	public void updateU(HashSet<Integer> keyDown, TEPanel v, double factor) {
		super.updateU(keyDown, v, factor);
		rotate += 0.02 / factor * Math.cos(TEPanel.counter / 200D);
	}

	@Override
	public void draw(Graphics2D g, Eye e) {
		g.setColor(e.transform(clr, new Vector3D(Math.cos(rotate), Math.sin(rotate), 1), 0));
		Vector3D v = e.toScreen(pos.clone().sub(Vector2D.fromAngle(rotate).scale(0.2)));
		int w = (int) e.sphereWidth(pos, 0.1) + 1, h = (int) (Math.abs(w * Math.cos(e.beta))) + 1;

// **************** STUFF FOR LOCK DRAWING ********************
// g.fillOval(e.psX(v.x) - w, e.psY(v.y) - w, 2 * w, 2 * w);
//
// g.fillPolygon(new int[] { e.psX(v.x), (int) (e.psX(v.x) + w * 2 *
// Math.cos(rotate)),
// (int) (e.psX(v.x) + w * 2 * Math.cos(rotate + 0.5)) }, new int[] {
// e.psY(v.y),
// (int) (e.psY(v.y) + w * 2 * Mathb.sin(rotate)), (int) (e.psY(v.y) + w * 2 *
// Math.sin(rotate + 0.5)) }, 3);

		if (v != null) {

			g.drawOval(e.psX(v.x) - w, e.psY(v.y) - h, 2 * w, 2 * h);

			int x1 = (int) (e.psX(v.x) + w * Math.cos(rotate)), y1 = (int) (e.psY(v.y) + h * Math.sin(rotate)), x2 = (int) (e
					.psX(v.x) + 4 * w * Math.cos(rotate)), y2 = (int) (e.psY(v.y) + 4 * h * Math.sin(rotate));

			g.drawLine(x1, y1, x2, y2);

			for (int i = 0; i < keys.length; i++) {
				g.drawLine(x1 + ((x2 - x1) * (2 * i + keys.length)) / (3 * keys.length),//
						y1 + ((y2 - y1) * (2 * i + keys.length)) / (3 * keys.length), // end
						x1 + ((x2 - x1) * (2 * i + keys.length)) / (3 * keys.length) + ((y2 - y1) * keys[i] * w)
								/ (h * 25), //
						y1 + ((y2 - y1) * (2 * i + keys.length)) / (3 * keys.length) + ((x1 - x2) * keys[i] * h)
								/ (w * 25));
			}
		}
	}

	@Override
	public Vector3D getCenter() {
		return pos;
	}

	@Override
	public Vector3D getNormal() {
		return new Vector3D(0, 0, 1);
	}

	@Override
	public void interaction(TEPanel t) {}
}
