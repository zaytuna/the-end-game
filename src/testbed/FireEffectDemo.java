package testbed;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.MemoryImageSource;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

import util.Methods;

public class FireEffectDemo extends JPanel implements Runnable {
	public static final int[] COLORS;
	public static double[] WEIGHTS = { 2, .5, .5, 1 };
	private static final long serialVersionUID = 886393697758105350L;
	private static Random RND = new Random();

	public double rate = 4.04;

	private final int WIDTH = 1800, HEIGHT = 400;
	private int[] pixels;
	private double[] fire, fire2;
	private MemoryImageSource source;
	public Image img;

	static int colormode = 3;

	boolean clock = true;

	static {
		COLORS = new int[256];
		for (int i = 0; i < 256; i++)
			COLORS[i] = findColor(i);

	}

	public FireEffectDemo() {
		pixels = new int[WIDTH * HEIGHT];
		fire = new double[WIDTH * HEIGHT];
		fire2 = new double[WIDTH * HEIGHT];

		Arrays.fill(pixels, Color.BLACK.getRGB());
		source = new MemoryImageSource(WIDTH, HEIGHT, pixels, 0, WIDTH);

		source.setAnimated(true);
		source.setFullBufferUpdates(true);

		img = createImage(source);

		setPreferredSize(new Dimension(WIDTH, HEIGHT));

		new Thread(this).start();
	}

	public void update() {
		clock = !clock;

		double[] src = clock ? fire : fire2;
		double[] dst = clock ? fire2 : fire;

		int size = RND.nextInt(20) + 10;
		for (int i = 0; i < WIDTH; i += size) {
			int number = RND.nextInt(256);
			for (int m = 0; m < size; m++)
				dst[(i + m) % WIDTH] = (number * .8 + src[(i + m) % WIDTH] * .2);
		}

		for (int j = HEIGHT - 1; j > 0; j--)
			for (int i = 0; i < WIDTH; i++) {
				dst[j * WIDTH + i] = ((src[j * WIDTH + i] + WEIGHTS[0] * src[(j - 1) * WIDTH + i] + WEIGHTS[1]
						* src[(j - 1) * WIDTH + (i + 1) % WIDTH] + WEIGHTS[2]
						* src[(j - 1) * WIDTH + (i - 1 + WIDTH) % WIDTH] + (j < 3 ? RND.nextInt(256) : WEIGHTS[3]
						* src[(j - 2) * WIDTH + i])) / (rate + 1));
				pixels[j * WIDTH + i] = COLORS[(int) dst[j * WIDTH + i]];
			}

		// if(Math.random() < .5)
		source.newPixels();
		repaint();
	}

	private static int findColor(int a) {
		// a = (int) (256 * Math.pow(a / 256D, .5));
		switch (colormode) {
		case 0:
			return (255 << 24) | (a << 8);
		case 1:
			if (a < 50)
				return Methods.colorMeld(Color.BLACK, Color.WHITE, Math.pow(a / 50D, 3)).getRGB();
			return Methods.colorMeld(Color.WHITE, Color.BLACK, (a - 50) / (206D)).getRGB();

		default:
			if (a < 50)
				return Methods.colorMeld(Color.BLACK, Color.RED, Math.pow(a / 50D, 3)).getRGB();
			else if (a < 100)
				return Methods.colorMeld(Color.RED, Color.ORANGE, (a - 50) / 50D).getRGB();
			else if (a < 150)
				return Methods.colorMeld(Color.ORANGE, Color.YELLOW, (a - 100) / 50D).getRGB();
			return Methods.colorMeld(Color.YELLOW, Color.WHITE, Math.pow((a - 150) / 106D, .05)).getRGB();
		}
	}

	public void run() {
		try {
			Thread.sleep(500);
		} catch (Exception e) {
		}
		while (true) {
			try {
				Thread.sleep(1 / 60);
			} catch (Exception e) {
			}
			update();
		}
	}

	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, this);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		FireEffectDemo fed = new FireEffectDemo();
		frame.add(fed);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
