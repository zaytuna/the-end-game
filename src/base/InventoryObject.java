package base;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashSet;

import util.Methods;
import math.Vector3D;

public abstract class InventoryObject extends GameObject implements Interactable {
	public static final Font FONT = new Font("SANS_SERIF", Font.PLAIN, 60);

	public double weight = 0;
	public String description = "A generic object that does not deserve a description";

	public Vector3D pos;
	public double radius = 0.5;
	public boolean reload = true;

	public abstract Image getIcon();

	@Override
	public void updateU(HashSet<Integer> keyDown, TEPanel v, double factor) {
		super.updateU(keyDown, v, factor);
		if (v.player.pos.dist2(pos) < radius * radius && reload) {
			v.slowmo = 0;
			reload = false;
			flag = 1;
			System.out.println("FLAGS"+flag);
			v.overlays.add(new NewItemMessage());
		} else if (!reload && (v.player.pos.dist2(pos) > radius * radius)) {
			reload = true;
		}
	}

	protected class NewItemMessage implements Overlay {

		public String text = "Look! Shiny!";
		private boolean dead = false;

		public void draw(Graphics2D g, TEPanel te) {
			g.setColor(new Color(0, 0, 0, 200));
			g.fill(g.getClip());
			g.setColor(Color.white);
			g.setFont(FONT);
			Methods.drawCenteredText(g, text, 0, 0, te.getWidth(), te.getHeight());
		}

		public void update(TEPanel tePanel) {
			if (tePanel.getKeys().contains(KeyEvent.VK_SPACE)) {
				dead = true;
				tePanel.slowmo = 1;
			}
		}

		public boolean isDead(TEPanel te) {
			return dead;
		}

		public void mousePress(MouseEvent evt) {}

		public void mouseRelease(MouseEvent evt) {}
	}
}
