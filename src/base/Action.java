package base;

public abstract class Action implements Reconstructable {
	public Trigger trigger;

	protected Action(Trigger t) {
		this.trigger = t;
	}

	public abstract void perform(TEPanel tp);
}
