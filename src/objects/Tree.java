package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import math.Matrix;
import math.Vector3D;
import util.Methods;
import base.Eye;
import base.GlobalOptions;
import base.RenderShard;
import base.TEPanel;

public class Tree extends CircleWall {
	public static int TREELIGHT = -1;
	ArrayList<Vine> vines = new ArrayList<Vine>();
	Vector3D start;
	double thickness;
	int cmode;

	public Tree(Vector3D a, double thick, int mode) {
		super(a.x, a.y, thick);
		Vine v = new Vine(a, Math.random() * 2 * Math.PI, 0, thick, 2, mode);
		v.flowerRate = 0.1;
		v.speed = 0.005;
		v.counter = -100;
		start = a.clone();
		vines.add(v);

		thickness = thick;
		cmode = mode;
	}

	@Override
	public String pack() {
		return start.pack() + "\t" + thickness + "\t" + cmode;
	}

	public static Tree read(String str) {
		String[] split = str.split("\t");
		return new Tree(Vector3D.unpack(split[0]), Double.parseDouble(split[1]), Integer.parseInt(split[2]));
	}

	@Override
	public void updateU(HashSet<Integer> keyDown, TEPanel p, double f) {
		super.updateU(keyDown, p, f);
		if (TEPanel.counter % 1 == 0)
			for (int i = 0; i < vines.size(); i++) {
				// System.out.println(i + "," + vines.get(i).saved.size());
				vines.get(i).update(f);
				vines.get(i).draw(f);
			}
	}

	public static class VineCircle implements RenderShard {
		Vector3D center, dir;
		double width;
		Color color;

		public VineCircle(Vector3D v, double w, Color c, Vector3D dir) {
			this.center = v;
			this.width = w;
			this.color = c;
			this.dir = dir;
		}

		@Override
		public void draw(Graphics2D g, Eye e) {
			Vector3D p1 = e.toScreen(center);

			if (p1 != null) {
				int w = e.sphereWidth(center, width);
// int w = 1;
				g.setColor(e.transform(color, dir, 1));

				if (GlobalOptions.ovals)
					g.fillOval((int) e.psX(p1.x) - w, (int) e.psY(p1.y) - w, 2 * w, 2 * w);
				else g.fillRect((int) e.psX(p1.x) - w, (int) e.psY(p1.y) - w, 2 * w, 2 * w);

				Vector3D p2 = e.toScreen(dir.clone().scale(0.5).add(center));
				if (p2 != null) {
					g.setColor(Color.white);
					g.drawLine((int) e.psX(p1.x), (int) e.psY(p1.y), (int) e.psX(p2.x), (int) e.psY(p2.y));
				}
			}
		}

		public ExpendableCircleRenderShard freeze(Eye e) {
			Vector3D p1 = e.toScreen(center);
			if (p1 == null)
				return null;
			return new ExpendableCircleRenderShard(e.transform(color, dir, TREELIGHT), null, (int) e.psX(p1.x),
					(int) e.psY(p1.y), (int) e.sphereWidth(center, width), center, e.clone().sub(center), true);
		}

		@Override
		public Vector3D getCenter() {
			return center;
		}

		@Override
		public Vector3D getNormal() {
			return new Vector3D(0, 0, 1);
		}
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);
		for (int i = 0; i < vines.size(); i++) {
			for (int j = 0; j < vines.get(i).saved.size(); j++)
				vines.get(i).saved.get(j).draw(g, e);

			vines.get(i).draw(g, e);
		}
	}

	public class Vine implements RenderShard {
		Color c1, c2, c;
		double width, speed = 0, flowerRate = 0.01;
		double ratio = 0;
		double counter = 0;
		double variance = 0;
		int colorMode;
		int index;

		Vector3D pos;
		double alpha, beta, aTo;

		ArrayList<VineCircle> saved = new ArrayList<VineCircle>();

		double powDetail = 0.45, wThresh = 90;

		// index < 0 ==> Flowering

		public Vine(Vector3D vec, double aa, double bb, double thick, double v, int colorMode) {
			this.pos = vec;
			this.width = thick;
			this.alpha = aa;
			this.beta = bb;
			this.variance = v;
			this.colorMode = colorMode;
			c1 = Color.BLACK;
			c2 = randomColor(colorMode, width);
		}

		public void update(double f) {
			counter += 1D / f;
			if (width >= 0) {
				double oldW = width;

				width *= Math.pow(0.998, 1D / f);
				// width *= 0.998;

				if (width < 0.01) {
					width = -1;
					return;
				}

				aTo = Math.atan2((pos.y - start.y), (pos.x - start.x)) + Methods.pseudoRandom(index);

				if (Math.abs(alpha - aTo) > Math.PI)
					if (alpha < aTo)
						aTo -= Math.PI * 2;
					else aTo += Math.PI * 2;

				alpha %= 2 * Math.PI;
				alpha += (aTo - alpha) / (10 * f);

				alpha += Math.cos(counter / 100D) / (f * (2 + 3 * Math.pow(width * 40, 1.5)));
				beta += Math.sin(counter / 22D) / (f * (3 + 4 * Math.pow(width * 50, 1.1)));

				beta += (0.9 * Math.PI / 2 - beta) / (f * (6 + Math.pow(width * 55, 3)));

				pos.add(Matrix.create3DEulerRotMatrix(alpha, beta, 0).applyTo(new Vector3D(0, 0, speed / f)));
				c = Methods.colorMeld(c1, c2, ratio);

				ratio += 0.01 / f;

				if (ratio >= 1) {
					c1 = c2;
					c2 = randomColor(colorMode, width);
					ratio = 0;
				}

				double diff = oldW % flowerRate - width % flowerRate;

				// System.out.println(width + "," + flowerRate + "," + width %
				// flowerRate);
				if (diff < 0 && counter > 50 && vines.size() < 1000) {
					// System.out.println("new flower; " + vines.size());
					Vine v = new Vine(pos.clone(), alpha + Math.random() * 2 * Math.PI, beta + Math.PI / 3
							* (2 * (int) (Math.random() * 2) - 1), width, 1, colorMode);
					v.c1 = c1;
					v.ratio = ratio;
					v.flowerRate = flowerRate * 0.75;
					v.speed = (Math.random() * 2 + 0.6) / 400;
					v.c2 = c2;
					v.counter = -(int) (width * 400);
					vines.add(v);
					v.index = vines.size() - 1;
				}
			}
		}

		public void draw(double f) {
			if (width > 0 && c != null) {
				double skipat100fps = Math.pow(width, powDetail) * wThresh;
				// skipat100fps = skipat100fps/2;
				if ((int) counter % (int) (skipat100fps + 1) == 0) {
					Vector3D v = Matrix.create3DEulerRotMatrix(alpha, beta, 0).applyTo(new Vector3D(0, 0, 1));
					saved.add(new VineCircle(pos.clone(), width, c, v));
				}
			}
		}

		@Override
		public void draw(Graphics2D g, Eye e) {
			if (width > 0) {
				Vector3D p1 = e.toScreen(pos);

				if (p1 != null) {
					int w = e.sphereWidth(pos, width);

					g.setColor(e.transform(c,
							Matrix.create3DEulerRotMatrix(alpha, beta, 0).applyTo(new Vector3D(0, 0, 1)), TREELIGHT));

					if (GlobalOptions.ovals)
						g.fillOval((int) e.psX(p1.x) - w, (int) e.psY(p1.y) - w, 2 * w, 2 * w);
					else g.fillRect((int) e.psX(p1.x) - w, (int) e.psY(p1.y) - w, 2 * w, 2 * w);
				}
			}
		}

		@Override
		public Vector3D getCenter() {
			return pos;
		}

		public Vector3D getNormal() {
			return new Vector3D(0, 0, 1);
		}
	}

	public static Color randomColor(int n, double w) {

		switch (n) {
		// ****************** GREEN *****************
		case 0: {
			int g = (int) (Math.random() * 255);
			return new Color((int) (Math.random() * g), g, (int) (Math.random() * g), 150);
		}
		// ******************* AUTUMN *******************
		case 1: {
			int r = 100 + (int) (Math.random() * 150), y = (int) (Math.random() * (r - 50));
			return new Color(r, y, (int) (Math.random() * (y * 3 / 4)));
		}
		// ******************* SPRING *******************
		default:
		case 2: {
			int g = 50 + (int) (Math.random() * 205), g2 = 50 + (int) (Math.random() * 205);
			return new Color((int) (Math.random() * Math.random() * (Math.min(g, g2))), g, g2);
		}
		case 3: {
			// ******************* TOPLOLOGICAL ***********
			return Methods.randomColor(10 + (int) (w * 100), 255);
		}
		case 4: {
			// ***************** TREEE
			if (w >= 0.07) {
				int r = (int) (Math.random() * 50);
				return new Color((int) (63 + r), 33 + (int) (r), (int) (r));
			}
			return randomColor(0, w);
		}
		case 5:
			int x = (int) (Math.random() * 255);
			return new Color(x, x, x);
		case 9:
			return Methods.randomColor();
		}
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> collector, Eye eye) {
		super.collectRenderShards(collector, eye);
		for (int i = 0; i < vines.size(); i++) {
			collector.add(vines.get(i));

			for (int q = 0; q < vines.get(i).saved.size(); q++) {
				RenderShard d = vines.get(i).saved.get(q).freeze(eye);
				if (d != null)
					collector.add(d);
			}
		}
	}
}
