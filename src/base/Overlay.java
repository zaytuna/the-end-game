package base;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

public interface Overlay  {
	public void draw(Graphics2D g, TEPanel te);
	public void update(TEPanel tePanel);
	public boolean isDead(TEPanel te);
	public void mousePress(MouseEvent evt);
	public void mouseRelease(MouseEvent evt);
}
