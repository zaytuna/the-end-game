package objects;

import math.Polygon2D;
import base.Action;
import base.Player;
import base.Reconstructor;
import base.Trigger;

public class PolygonalTrigger extends Trigger {
	Polygon2D shape;

	public PolygonalTrigger(double... things) {
		shape = new Polygon2D();

		for (int i = 1; i < things.length - 1; i += 2) {
			shape.addPoint(things[i], things[i + 1]);
		}
	}

	public static PolygonalTrigger read(String str) {
		String[] split = str.split("~");
		String[] polyPieces = split[0].split("^");
		double[] doubles = new double[polyPieces.length];

		for (int i = 0; i < doubles.length; i++)
			doubles[i] = Double.parseDouble(polyPieces[i]);

		PolygonalTrigger ct = new PolygonalTrigger(doubles);

		try {
			for (int n = 1; n < split.length; n++) {
// Actions can have \t or : as property delimits. First | is the class name

				String rest = split[n].substring(split[n].indexOf("|") + 1);
				String name = split[n].substring(0, split[n].indexOf("|"));
				Action a;

				a = (Action) Reconstructor.unpack(Class.forName(name), rest, null);

				a.trigger = ct;
				ct.attached.add(a);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return ct;
	}

	@Override
	public boolean condition(Player p) {
		return shape.contains(p.pos.x, p.pos.y);
	}

	@Override
	public String pack() {
		StringBuilder sb = new StringBuilder();
				
		for(int i = 0; i < shape.npoints; i++)
			sb.append("^"+shape.xpoints[i]+"^"+shape.ypoints[i]);
		sb.deleteCharAt(0);

		for (int i = 0; i < attached.size(); i++)
			sb.append("~" + attached.get(i).pack());

		return sb.toString();
	}
}
