package objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import base.Eye;
import base.GlobalOptions;
import base.RenderShard;
import math.Vector3D;

public class ExpendableCircleRenderShard implements RenderShard {
	int x, y, r;

	boolean filled = true;

	Vector3D center, norm;
	Color c;
	Stroke k;

	public ExpendableCircleRenderShard(Color c, Stroke k, int x1, int y1, int r, Vector3D q, Vector3D n, boolean filled) {
		this.x = x1;
		this.r = r;
		this.y = y1;
		this.k = k;
		this.c = c;
		this.center = q;
		this.norm = n;
		this.filled = filled;
	}

	public ExpendableCircleRenderShard(int x1, int y1, int r, Vector3D q, Vector3D n, boolean filled) {
		this(null, null, x1, y1, r, q, n, filled);
	}

	@Override
	public void draw(Graphics2D g, Eye e) {
		if (c != null)
			g.setColor(c);
		if (k != null)
			g.setStroke(k);

		if (GlobalOptions.ovals) {
			if (filled)
				g.fillOval(x - r, y - r, 2 * r, 2 * r);
			else g.drawOval(x - r, y - r, 2 * r, 2 * r);
		} else {
			if (filled)
				g.fillRect(x - r, y - r, 2 * r, 2 * r);
			else g.drawRect(x - r, y - r, 2 * r, 2 * r);
		}
	}

	@Override
	public Vector3D getCenter() {
		return center;
	}
	
	@Override
	public Vector3D getNormal() {
		return norm;
	}

}
