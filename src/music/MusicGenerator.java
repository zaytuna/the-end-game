package music;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;

public class MusicGenerator {
	Synthesizer synth;
	int[] instruments = { 0, 1, 2, 3, 114, 100, 117, 87 };

	public MusicGenerator() {
		try {
			synth = MidiSystem.getSynthesizer();
			synth.open();

// for (int i = 0; i <235; i++)
// System.out.println(i + "\t" +synth.getAvailableInstruments()[i].getName());

			MusicPiece music = new MusicPiece();

			for (int i = 0; i < instruments.length; i++) {
				DynamicPart part = new DynamicPart(instruments[i], music, i);
				// i < 4 ? 1 : i == 4 ? 114 : (int) (Math.random() * 120)

				if (i == 5) {
					part.job = 2;
					part.id = 0;
				}
				// (int) (Math.random() * 120)
				part.play(synth, (i % synth.getChannels().length));
				Thread.sleep(4800);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		new MusicGenerator();
	}
}
