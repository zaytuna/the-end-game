package objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import physics.ForceApplier;
import physics.PhysicsObject;
import physics.PolyPObject;
import math.Polygon2D;
import math.Vector2D;
import math.Vector3D;
import util.Methods;
import base.Action;
import base.Eye;
import base.GameObject;
import base.Player;
import base.Reconstructor;
import base.RenderShard;
import base.TEPanel;
import base.Trigger;
import base.WorldSegment;

public class HWall extends GameObject implements ForceApplier, RenderShard {
	protected double x1, x2, y1, y2;
	protected Color color = Color.BLACK;
	protected float width = 5;
	double friction = 1, bounce = 0.1; // (-.1 -> 1 --> 1.5);
	protected boolean electric = false;

	protected boolean operational = true;
	protected boolean spiked = false;

	protected double height = -0.03, length = 0;

	ArrayList<GameShape> associated = new ArrayList<GameShape>();

	public HWall(double a, double b, double c, double d) {
		x1 = a;
		x2 = c;
		y1 = b;
		y2 = d;
		length = Math.sqrt((a - c) * (a - c) + (b - d) * (b - d));
	}

	public void setOperational(boolean b) {
		operational = b;
		create(2);
	}

	public ArrayList<GameShape> create(double h) {
		ArrayList<GameShape> shapes = new ArrayList<GameShape>();
		if (operational) {
			double dx = 0.5 / Vector2D.dist(x1, y1, x2, y2);
			for (double d = 0; d < 1; d += dx * 1.1) {
				if (1 - d < dx)
					dx = 1 - d;

				shapes.add(new GameShape(color, true, lini(x1, x2, d), lini(y1, y2, d), 0, lini(x1, x2, d + dx), lini(
						y1, y2, dx + d), 0, lini(x1, x2, dx + d), lini(y1, y2, d + dx), h, lini(x1, x2, d), lini(y1,
						y2, d), h));
			}
		} else {

			shapes.add(new GameShape(Methods.getColor(color, 100), false, x1, y1, 0.1, x2, y2, 0.1, x2, y2, h, x1, y1,
					h));
		}
		children.clear();
		children.addAll(shapes);
		return shapes;
	}

	protected static double lini(double a, double b, double p) {
		return a + (b - a) * p;
	}

	@Override
	public void backupDrawU(Graphics2D g, Eye e) {
		super.backupDrawU(g, e);
		draw(g, e);
	}

	@Override
	public String pack() {
		String str = x1 + "\t" + y1 + "\t" + x2 + "\t" + y2 + "\t" + electric + "\t" + friction + "\t" + width + "\t"
				+ color.getRGB() + "\t" + bounce + "\t" + operational;

		return str;
	}

	public void draw(Graphics2D g, Eye e) {
		Vector3D v1 = e.toScreen(new Vector3D(x1, y1, height)), v2 = e.toScreen(new Vector3D(x2, y2, height));
		if (v1 != null && v2 != null) {
			g.setStroke(operational ? new BasicStroke(width) : new BasicStroke(width, BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_MITER, 10.0f, new float[] { width * 2 }, TEPanel.counter / 1f));
			g.setColor(operational ? e.transform(color, null, 0) : e.transform(Methods.getColor(color, 100), null, 0));

			g.drawLine(e.psX(v1.x), e.psY(v1.y), e.psX(v2.x), e.psY(v2.y));
			g.setStroke(new BasicStroke(1));

			if (spiked && operational) {
				Vector2D d = v2.to2DV().sub(v1.to2DV());
				double m = d.mag();
				d.unitize();
				Vector2D dp = d.clone().perp().scale(width / 250);

				for (float i = 0; i < m; i += m / (2 * length)) {
					Vector2D p = d.clone().scale(i);
					g.drawLine(e.psX(v1.x + p.x - dp.x), e.psY(v1.y + p.y - dp.y), e.psX(v1.x + p.x + dp.x),
							e.psY(v1.y + p.y + dp.y));
				}
			}

		}
	}

	public HWall enable(boolean b) {
		operational = b;
		return this;
	}

	public void update(HashSet<Integer> keyDown, TEPanel t, double f) {

	}

	public void magic(PhysicsObject po) {
		if (operational /*|| !(po instanceof Player)*/) {
			Vector2D d = new Vector2D(x2 - x1, y2 - y1), pp = po.pos.to2DV().minus(new Vector2D(x1, y1));
			Vector2D prd = pp.projOnto(d);

			pp.minus(prd);

			if (electric && prd.mag() <= d.mag() && prd.dot(d) > 0) {
				po.velo.add(pp.scale(0.004 / (pp.mag2() * pp.mag2() + .01)));
				return;
			}

			if (po instanceof PolyPObject) {
				PolyPObject
						.linePhysics2D((PolyPObject) po, friction, x1, y1, x2, y2, pp.to3DV(), bounce, Vector3D.ZERO);
				return;
			}

			// this is for circular physics, then?
			// make sure object center lies within the bounds

			// this is relative to (x1,y1)

			if (prd.mag() <= d.mag() + po.radius && prd.dot(d) > -po.radius) {
				Vector2D plat = d.clone();
				d.perp();
				if (pp.dot(d) < 0)
					d.negate();

				if (pp.mag() < po.radius) {
					if (spiked)
						po.damage(1);

					if (!(po instanceof PolyPObject)) {

						po.pos.add(d.unitize().clone().scale(po.radius - pp.mag()));
						po.norm = d.clone().to3DV();

						Vector2D q = po.velo.to2DV().projOnto(plat), r = po.velo.to2DV().minus(q);
						q.add(r.scale(-bounce));
						po.velo.x = q.x;
						po.velo.y = q.y;

						po.friction = friction;
					}

				}
			}

		}
	}

	public static HWall read(String str) {
		String[] pieces = str.split("\t");
		// System.out.println(str);
		HWall w = new HWall(Double.parseDouble(pieces[0]), Double.parseDouble(pieces[1]),
				Double.parseDouble(pieces[2]), Double.parseDouble(pieces[3]));

		w.electric = pieces[4].charAt(0) == 't';
		w.friction = Double.parseDouble(pieces[5]);
		w.width = Float.parseFloat(pieces[6]);
		w.color = new Color(Integer.parseInt(pieces[7]), true);
		w.bounce = Double.parseDouble(pieces[8]);
		w.setOperational(pieces[9].startsWith("t"));

		return w;
	}

	@Override
	public Vector3D getCenter() {
		return new Vector3D((x1 + x2) / 2, (y1 + y2) / 2, height);
	}
	
	@Override
	public Vector3D getNormal() {
		return new Vector3D(y1-y2, x2-y1, 0);
	}

	@Override
	public void collectRenderShards(Collection<RenderShard> c, Eye e) {
		super.collectRenderShards(c, e);
		c.add(this);
	}

	public static class WallTrigger extends Trigger {
		HWall parent;
		double dist;
		Polygon2D calculated;

		public WallTrigger(HWall h, double d) {
			this.parent = h;
			this.dist = d;
			recalculate();
		}

		@Override
		public String pack() {
			StringBuilder built = new StringBuilder();
			for (int i = 0; i < attached.size(); i++)
				built.append("~" + attached.get(i).pack());
			return parent.name + "~" + dist + built.toString();
		}

		public void recalculate() {
			Vector2D dir = new Vector2D(parent.x2 - parent.x1, parent.y2 - parent.y1), perp = dir.clone().perp()
					.unitize().scale(dist);
			calculated = new Polygon2D(new double[] { parent.x1 + perp.x, parent.x2 + perp.x, parent.x2 - perp.x,
					parent.x1 - perp.x }, new double[] { parent.y1 + perp.y, parent.y2 + perp.y, parent.y2 - perp.y,
					parent.y1 - perp.y }, 4);
		}

		@Override
		public boolean condition(Player p) {
			return calculated.contains(p.pos.x, p.pos.y);
		}

		public static WallTrigger read(String str, WorldSegment ws) {
			String[] split = str.split("~");
			WallTrigger wt = new WallTrigger((HWall) ws.get(split[0]), Double.parseDouble(split[1]));

			try {
				for (int i = 2; i < split.length; i++) {
// Actions can have \t or : as property delimits. The first | is the class
					String rest = split[i].substring(split[i].indexOf("|") + 1);
					String name = split[i].substring(0, split[i].indexOf("|"));
					Action a;

					a = (Action) Reconstructor.unpack(Class.forName(name), rest, null);

					a.trigger = wt;
					wt.attached.add(a);
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return wt;
		}
	}
}
