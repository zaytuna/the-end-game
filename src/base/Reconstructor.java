package base;

import java.lang.reflect.Method;

public class Reconstructor {
	@SuppressWarnings("unchecked")
	public static <T> T unpack(Class<T> c, String str, WorldSegment ws) {
		try {
			try {
				Method m = c.getMethod("read", String.class);
				return (T) m.invoke(null, str);
			} catch (NoSuchMethodException e) {
				Method m = c.getMethod("read", String.class, WorldSegment.class);
				return (T) m.invoke(null, str, ws);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
