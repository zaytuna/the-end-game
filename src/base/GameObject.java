package base;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public abstract class GameObject implements Reconstructable {
	// **************** PROPERTIES OF ALL GAME OBJECTS ********************
	public boolean visible = true;
	int flag = 0; // do nothing at flag == 0;
					// remove at flag == 1;
	public WorldSegment home;
	public String name;
	// *********************************************************************

	// ************* TO MAKE THIS A CULLING TREE STRUCTURE *****************
	public ArrayList<GameObject> children = new ArrayList<GameObject>();
	public GameObject parent;

	// *********************************************************************
	public void collectRenderShards(Collection<RenderShard> collector, Eye eye) {
		for (GameObject c : children)
			c.collectRenderShards(collector, eye);
	}

	public void backupDrawU(Graphics2D g, Eye e) {
		for (GameObject c : children)
			c.backupDrawU(g, e);
	}
	

	public final void fakeBackupDraw(Graphics2D g, Eye e) {
		ArrayList<RenderShard> r = new ArrayList<RenderShard>();
		collectRenderShards(r, e);
		for(RenderShard s : r)
			s.draw(g, e);
	}

	// getBounds
	// public abstract boolean cullable(Eye e);
	// the previous method is absolutely necessary for performance. Must be
// implemented to get tree benefits.

	/**
	 * Called every frame on all game objects
	 * 
	 * @param factor
	 */
	public void updateU(HashSet<Integer> keyDown, TEPanel v, double factor) {
		for (GameObject c : children)
			c.updateU(keyDown, v, factor);
	}

	public void damage(double d) {}
}
