package music;

public class Rhythm {
	int timeLength = 1000; // total length ... in ms?
	int[] power;

	private Rhythm(int... s) {
		power = s;
	}

	public static Rhythm createBasic() {
		return new Rhythm(1);
	}

	public void subdivide(int n) {
		int[] np = new int[power.length * n];
		for (int i = 0; i < power.length; i++)
			np[i * n] = power[i];
		this.power = np;
	}

	public void trySimplify() {

	}

	public static Rhythm createRandom(int possDivisions) {
//		int i = 0;
		return new Rhythm();
	}

	public static Rhythm createFractal(int base, int levels, int range, double divProb) {
		int[] pow = new int[(int) Math.pow(base, levels)];
		fractal(pow, 0, pow.length, base, levels, range, divProb);
		return new Rhythm(pow);
	}

	private static void fractal(int[] array, int start, int end, int base, int levels, int range, double p) {
		if(levels == 0)
			return;
		for(int i = 0; i < base; i++)
		{
			fractal(array, start, end, base, levels-1, range, p);
		}
	}
}
